import { Component } from '@angular/core';

// The app component is always loaded in the window,
// then the navbar is always displayed on top with <nav-bar> selector
@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
            <nav-bar></nav-bar>
            <div class="main-window">
              <router-outlet></router-outlet>
            </div>
            `,
  styleUrls: ['styles/app.component.css'
]
})
export class AppComponent { }
