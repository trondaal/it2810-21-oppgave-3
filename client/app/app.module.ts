// All the angular components are imported here
import { NgModule, ValueProvider }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

// All components and services are imported here
import { AppComponent }   from './app.component';
import { NavbarComponent } from './components/navbar.component';
import { MovieListComponent } from './components/movie-list.component';
import { MovieDetailComponent } from './components/movie-detail.component';
import { MovieSearchComponent } from './components/movie-search.component';
import { HomePageComponent } from './components/homepage.component';
import { ProfileComponent } from './components/profile.component';
import { MovieService } from './services/movie.service';
import { UserService } from './services/user.service';
import { HttpClient } from './services/http.service';
import { ToastModule } from 'ng2-toastr/ng2-toastr';

const LocalStorageProvider: ValueProvider = {
    provide: Storage,
    useValue: window.localStorage
};

const WindowProvider: ValueProvider = {
    provide: Window,
    useValue: window
};

let options: any = {
  animate: 'flyRight',
  showCloseButton: true,
  toastLife: 4000,
  positionClass: 'toast-top-right',
};

@NgModule({
  imports:      [
    ToastModule.forRoot(options),
    BrowserModule,
    FormsModule,
    HttpModule,
    // All the different routes is declared here
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/homepage',
        pathMatch: 'full'
      },
      {
        path: 'list/:id',
        component: MovieListComponent
      },
      {
        path: 'homepage',
        component: HomePageComponent
      },
      {
        path: 'movie-detail/:id',
        component: MovieDetailComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'search/:value',
        component: MovieSearchComponent
      }
    ])
  ],
  // All our components are declared here
  declarations: [
    AppComponent,
    NavbarComponent,
    MovieListComponent,
    MovieDetailComponent,
    HomePageComponent,
    ProfileComponent,
    MovieSearchComponent
  ],
  // All our services are declared here
  providers: [
    MovieService,
    UserService,
    LocalStorageProvider,
    HttpClient,
    WindowProvider
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
