import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

// Handles token information sent to the database in a header section
@Injectable()
export class HttpClient {
  constructor(private http: Http, private localStorage: Storage) {
  }

  addHeaders(headers:Headers) {
    const token = this.localStorage.getItem("token");
    if (token) {
      headers.append('Authorization', token);
    }
    headers.set('content-type', 'application/json');
  }

  get(url, headers = new Headers()) {
    this.addHeaders(headers);
    return this.http.get(url, {
      headers: headers
    });
  }

  post(url, data, headers = new Headers()) {
    this.addHeaders(headers);
    return this.http.post(url, data, {
      headers: headers
    });
  }

  delete(url, headers = new Headers()) {
    this.addHeaders(headers);
    return this.http.delete(url, {
      headers: headers
    });
  }
}
