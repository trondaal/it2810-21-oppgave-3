import { Injectable } from '@angular/core';
import { HttpClient } from './http.service';
import { Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { Movie } from '../classes/movie';
import { User } from '../classes/user';

export interface LoginResponse {
  success: boolean;
  token: string;
}

interface RegisterResponse {
  success: boolean;
  error: string;
}

// Handles all the requests to the database regarding the user
@Injectable()
export class UserService {

  user: User;

  baseUrl: string = "/api/v1";

  constructor(private http: HttpClient){}

  // Registers a user
  registerUser(name: string, password: string, email: string): Promise<RegisterResponse> {
    return this.http.post(this.baseUrl+"/user", JSON.stringify({username: name, password: password, email: email})).toPromise().then(res => res.json());
  }

  // Logs in a user
  loginUser(name: string, password: string): Promise<LoginResponse> {
    return this.http.post(this.baseUrl+"/user/login", JSON.stringify({username: name, password: password})).toPromise().then(res => res.json());
  }

  // Retrieves the search hitory for the user
  getSearchHistory(): Promise<any> {
    return this.http.get(this.baseUrl+"/user/history").toPromise().then(res => res.json());
  }

  // Retrieves the movie lists the user has
  getUserLists(): Promise<any> {
    return this.http.get(this.baseUrl+"/list").toPromise().then(res => res.json());
  }

  // Retrieves all the movies in a certain list that the user has
  getMoviesInList(listId: number): Promise<Movie[]> {
    return this.http.get(this.baseUrl+"/list/"+listId).toPromise().then(res => res.json());
  }

  // Deletes a movie from the users selected list
  deleteMovieInList(listId: number, movieId:number): Promise<any> {
    return this.http.delete(`${this.baseUrl}/list/${listId}/${movieId}`).toPromise().then(res => res.json);
  }

  // Adds a movie to the users seleced list
  addMovieToList(listid: number, movieid: number): Promise<any> {
    return this.http.post(this.baseUrl+"/list/"+listid, JSON.stringify({movieId: movieid})).toPromise().then(res => res.json());
  }

}
