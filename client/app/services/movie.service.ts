import { Injectable } from '@angular/core';
import { Movie } from '../classes/movie';
import { HttpClient } from './http.service';
import 'rxjs/add/operator/toPromise';

// Handles all the requests for movies
@Injectable()
export class MovieService {

  movies: Movie[];
  baseUrl: string = "/api/v1/movie";

  constructor(private http: HttpClient) {}

  // Gets popular movies from the API
  getMovies(): Promise<Movie[]> {

    return this.http.get(`${this.baseUrl}/popular`).toPromise().then(res => res.json());
  }

  // Gets a certain movie by movieid from the API
  getMovie(id: number): Promise<Movie> {
    return this.http.get(`${this.baseUrl}/${id}`).toPromise().then(res => res.json());
  }

  // Searches for a movie and recieves a list of movies from the API
  search(value: string, page: number, pr0n: boolean = false): Promise<Movie[]> {
    var url = `${this.baseUrl}/search?query=${value}&page=${page}&adult=${pr0n}`;
    return this.http.get(url).toPromise().then(res => res.json());
  }
}
