import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

import { Movie } from '../classes/movie'
import { UserService } from '../services/user.service';

// This component displays a list of movies
@Component({
  moduleId: module.id,
  selector: 'movie-list',
  templateUrl: '../templates/movie-list.component.html',
  styleUrls: ['../styles/movie-list.component.css',
  '../styles/animate.css']
})

export class MovieListComponent {
  movies: Movie[];
  originalList: Movie[];
  title: string = "List";
  filters: boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private localStorage: Storage
  ){}

  // Sort a list
  onSort(value: number): void {
    switch(value) {
      case 0:
        this.movies = this.originalList.slice(0);
        break;
      case 1:
        this.movies = this.movies.sort((a, b) => b.year - a.year);
        break;
      case 2:
        this.movies = this.movies.sort((a, b) => b.rank - a.rank);
        break;
      case 3:
        this.movies = this.movies.sort((a, b) => {
          if (a.title < b.title) {
            return -1;
          }
          if (a.title > b.title) {
            return 1;
          }
          return 0;
        });
        break;
    }
  }

  hasNext(): boolean {
    return false;
  }

  hasPrevious(): boolean {
    return false;
  }

  // Filters the movies
  onFilterByRating(value: number): void {
    if(value == 0) {
      this.movies = this.originalList.slice(0);
    }
    else {
      this.movies = this.originalList.filter(m => m.rank >= value);
    }
    console.log(this.movies);
  }

  // When a movie is clicked on, the app displays the movie detail for the selected movie
  onSelect(movie: Movie): void {
    this.router.navigate(['/movie-detail', movie.id]);
  }

  // Fetches all the movies and appends them to a list
  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let listId = params['id'];
      this.userService.getMoviesInList(listId).then(movies => {
        this.movies = movies;
        this.originalList = movies.slice(0);
      }).catch(e => {
        this.movies = [];
        this.originalList = [];
      });
    });
  }
}
