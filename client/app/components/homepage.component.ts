import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Movie } from '../classes/movie'
import { MovieService } from '../services/movie.service';

// The homepage component displays the opening page for the application
@Component ({
  moduleId: module.id,
  selector: 'homepage',
  templateUrl: '../templates/homepage.component.html',
  styleUrls: [
    '../styles/homepage.component.css',
    '../styles/animate.css'
  ]
})

export class HomePageComponent {

  movies: Movie[];

  constructor(
    private router: Router,
    private movieService: MovieService
  ){}


  // When a movie is selected, the app routes to the movie detail view with the movie selected
  onSelect(movie: Movie): void {
    this.router.navigate(['/movie-detail', movie.id]);
  }
  // When the page is loaded it retrieves popular movies from the server
  ngOnInit(): void {
    this.movieService.getMovies().then(movies => this.movies = movies);
  }

}
