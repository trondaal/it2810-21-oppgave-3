import { Component, OnInit } from '@angular/core';
import { Movie } from '../classes/movie'
import { MovieService } from '../services/movie.service';
import { Router } from '@angular/router';

import { User } from '../classes/user';
import { UserService } from '../services/user.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

// The component displaying the navbar
@Component({
  moduleId: module.id,
  selector: 'nav-bar',
  templateUrl: '../templates/navbar.component.html',
  styleUrls: ['../styles/navbar.component.css']
})

export class NavbarComponent {

  constructor(
    private movieService: MovieService,
    private userService: UserService,
    private router: Router,
    public toastr: ToastsManager
  ) {}

  // List of variables used to change the buttons available
	user_email: string;
	user_password: string;
	user_name: string;

	show_main: string = ''
	show_login: string = '';
	show_register: string = '';
	show_reset: string = '';

	tab_login: string = '';
	tab_singup: string = '';

	select_login: string = '';
	select_register: string = '';
	show_password: string = 'hide-password';
	show_password_text: string = 'Show';
	password_type: string = 'password';


	toggle_error_email: boolean = false;
	toggle_error_username: boolean = false;
	toggle_error_password: boolean = false;

	toggle_login: boolean;
	toggle_logout: boolean;

  // Checks if the user is logged in when page is refreshed
  ngOnInit() {
    if (localStorage.getItem("token")) {
      this.toggle_login = false;
      this.toggle_logout = true;
    }
    else {
      this.toggle_login = true;
      this.toggle_logout = false;
    }
  }

  // Logs in a user
  onLogin() {
    this.userService.loginUser(this.user_name, this.user_password).then(response => {

      // Store some information about the user in the localStorage
      localStorage.setItem("token", response.token);
      localStorage.setItem("username", this.user_name);
      this.userService.getUserLists().then(response => {
        localStorage.setItem('lists', JSON.stringify(response));
      }).catch(response => {
        console.log(response);
      });


      this.user_name = null;
      this.user_password = null;
      this.user_email = null;

      this.toggle_error_username = false;
      this.toggle_error_email = false;
      this.toggle_error_password = false;

      this.toggle_login = false;
      this.toggle_logout = true;

      this.show_main = this.show_login = this.show_register = this.show_reset = '';

      this.toastr.success('Logged in!');
    }).catch(response => {
      this.toggle_error_password = true;
    });

  }

  // Registers a user and then logs in
  onRegister() {
    this.toggle_error_username = false;
    this.toggle_error_email = false;

    this.userService.registerUser(this.user_name, this.user_password, this.user_email).then(response => {
      this.onLogin();

      this.toastr.success('You are now registered!');
    }).catch(response => {

      if (response._body.includes("That username is already taken")) {
        this.toggle_error_username = true;
      }
      else if (response._body.includes("That email is already taken")) {
        this.toggle_error_email = true;
      }
      else if (response._body.includes("That username and email is already taken")) {
        this.toggle_error_username = true;
        this.toggle_error_email = true;

      }
    });

  }

  // Opens the dialog and selects page 'login'
	login() {
		this.show_main = this.show_login = 'is-visible';
    	this.show_register = this.select_register = this.show_reset = '';
    	this.select_login = 'selected';
    }

    // Opens the dialog and selects page 'register'
    register() {
    	this.show_main = this.show_register = 'is-visible';
    	this.show_login = this.show_reset = this.select_login = '';
    	this.select_register = 'selected';
    }

    // Logs out the user
    logout() {
      // Deletes the information stored about the user in the local storage
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      localStorage.removeItem("favid");
      localStorage.removeItem("haveid");
      localStorage.removeItem("planid");


      this.toggle_login = true;
      this.toggle_logout = false;

      this.toastr.info('You are now logged out.');
    }

    // Toggles the type of password and infotext
    toggle_password() {
    	( (this.show_password_text == 'Hide') ? this.show_password_text = 'Show' : this.show_password_text = 'Hide');
    	( (this.password_type == 'text') ? this.password_type = 'password' : this.password_type = 'text');
    }

    // Opens the reset page in the dialog window
    reset() {
    	this.show_main = this.show_reset = 'is-visible';
    	this.show_login = this.show_register = this.select_register = '';
    	this.select_login = 'selected';
    }
    // Close the dialog box.
    close(event) {
        var target = event.target;
        if (!target.closest(".cd-user-modal-container")) {
            this.show_main = this.show_login = this.show_register = this.show_reset = '';
        }
    }

    keyPress(event) {
	    var isEscape = false;
	    if ("key" in event) {
	        isEscape = (event.key == "Escape" || event.key == "Esc");
	    } else {
	        isEscape = (event.keyCode == 27);
	    }
	    if (isEscape) {
            this.show_main = this.show_login = this.show_register = this.show_reset = '';
	    }
    }


  onSubmit(value: string) {
    if(value.length > 0) {
      this.router.navigate(['/search', value]);
    }
  }


  // List of Toastr messages

  showSuccess() {
    this.toastr.success('Logged in!');
  }

  showError() {
    this.toastr.error('This is not good!', 'Oops!');
  }

  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }

  showCustom() {
    this.toastr.custom('<span style="color: red">Message in red.</span>', null, {enableHTML: true});
  }

}
