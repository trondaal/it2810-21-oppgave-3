import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { UserService } from '../services/user.service';
import { MovieService } from '../services/movie.service';
import { Movie } from "../classes/movie";

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

// This component displays a movie in detail
@Component({
  moduleId: module.id,
  selector: 'movie-detail',
  templateUrl: '../templates/movie-detail.component.html',
  styleUrls: [
    '../styles/movie-detail.component.css'
  ]
})

export class MovieDetailComponent {
  @Input()
  movie: Movie;

  lists: any[];

  constructor (
    private route: ActivatedRoute,
    private movieService: MovieService,
    private userService: UserService,
    private toastr: ToastsManager
  ){}

  // Adds a movie to the users selected list
  // Also handles if the user is not logged in, or if the movie is already in the list
  addToList(list) {
    if(localStorage.getItem("token")) {
      this.userService.addMovieToList(list.id, this.movie.id).then(response => {
        this.toastr.info('The movie was added!');
      }).catch(response => {
        let error = JSON.parse(response._body);
        this.toastr.warning(error.error, 'Alert!');
      });
    }
    else {
      this.toastr.warning('You are not logged in!', 'Alert!');
    }
  }

  // Retrieves all the details an images for the movie to be displayed
  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.movieService.getMovie(id).then(movie => this.movie = movie);
    });
    this.lists = JSON.parse(localStorage.getItem('lists'));
    console.log(this.lists);
  }

}
