import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

import { Movie } from '../classes/movie'
import { MovieService } from '../services/movie.service';

// This component handles a movie search
@Component({
  moduleId: module.id,
  selector: 'movie-list',
  templateUrl: '../templates/movie-list.component.html',
  styleUrls: ['../styles/movie-list.component.css']
})

export class MovieSearchComponent {
  movies: Movie[];
  originalList: Movie[];
  title: string = "Search results";
  filters: boolean = true;
  value: string = "";
  page: number = 1;
  adult: boolean = false;

  orderBy: number = 0;
  minRank: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private window: Window,
    private movieService: MovieService
  ){}

  // When a movie is selected the selected movie is displayed in a movie detail view
  onSelect(movie: Movie): void {
    this.router.navigate(['/movie-detail', movie.id]);
  }

  // Goes to next page
  nextPage() {
    this.page++;
    this.search();
    this.window.scrollTo(0,0);
  }

  // Goes to previous page
  prevPage() {
    this.page--;
    this.search();
    this.window.scrollTo(0,0);
  }

  // Toggles adult filter
  toggleAdult() {
    this.adult = !this.adult;
    this.search();
  }

  // Searches for a movie from user input
  search() {
    this.movieService.search(this.value, this.page, this.adult).then(movies => {
      this.originalList = movies.slice(0);
      this.onSort(this.orderBy);
      this.onFilterByRating(this.minRank);
    }).catch(e => {
      this.movies =[];
      this.originalList = [];
    });
  }

  hasNext(): boolean {
    return this.originalList && this.originalList.length == 20;
  }

  hasPrevious(): boolean {
    return this.page > 1;
  }

  // Sorts movies by parameter
  onSort(value: number): void {
    this.orderBy = value;
    switch(value) {
      case 0:
        this.movies = this.originalList.slice(0);
        break;
      case 1:
        this.movies = this.movies.sort((a, b) => b.year - a.year);
        break;
      case 2:
        this.movies = this.movies.sort((a, b) => b.rank - a.rank);
        break;
      case 3:
        this.movies = this.movies.sort((a, b) => {
          if (a.title < b.title) {
            return -1;
          }
          if (a.title > b.title) {
            return 1;
          }
          return 0;
        });
        break;
    }
  }

  // Filter by rating
  onFilterByRating(value: number): void {
    this.minRank = value;
    if(value == 0) {
      this.movies = this.originalList.slice(0);
    }
    else {
      this.movies = this.originalList.filter(m => m.rank >= value);
    }
    console.log(this.movies);
  }

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      this.value = params['value'];
      this.search();
    });
  }
}
