import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../services/user.service';

import { Movie } from '../classes/movie';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

// The component displaying the user information and lists
@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: '../templates/profile.component.html',
  styleUrls: ['../styles/profile.component.css']
})

export class ProfileComponent {
  lists: any[];
  movies: Movie[];
  list: any;
  history: any[];
  showHistory: boolean = false;
  user_name: string;
  showmeta: boolean = true;
  movie_class: string = "movie";
  image_src: string = "app/img/images.svg";

  constructor(
    private router: Router,
    private userService: UserService,
    private localStorage: Storage,
    private toastr: ToastsManager
  ){}

  // Toggles the detail for the movies in the users lists
  toggleDetail() {
    if (this.showmeta === false) {
      this.showmeta = true;
      this.movie_class = "movie";
      this.image_src = "app/img/images.svg";
    }
    else {
      this.showmeta = false;
      this.movie_class = "movie-image";
      this.image_src = "app/img/listing-option.svg";
    }
  }

  onSelect(list: any): void {
    this.userService.getMoviesInList(list.id).then(movies => this.movies = movies).catch(e => console.log);
    this.list = list;
    this.showHistory = false;
  }

  // Sends the user to a detailed view for the movie selected
  onSelectMovie(movie: Movie): void {
    this.router.navigate(['/movie-detail', movie.id]);
  }

  // Deletes a movie from the users list
  onDelete(movie: Movie): void {
    this.movies = this.movies.filter(m => m.id != movie.id);
    this.userService.deleteMovieInList(this.list.id, movie.id).catch(e => console.log);

    this.toastr.info('Deleted movie ' + movie.title + ' from ' + this.list.name);
  }

  // If a query from the search history is clicked, the query is used in a search
  onSearch(query): void {
    this.router.navigate(['/search', query]);
  }

  onShowSearchHistory(): void {
    this.userService.getSearchHistory().then(res => this.history = res.searchQueries).catch(e => console.log);
    this.showHistory = true;
  }

  // If the user is not logged in, the user is sent back to the homepage
  ngOnInit() {
    if (localStorage.getItem("token")) {
      this.lists = JSON.parse(this.localStorage.getItem("lists"));
      this.onSelect(this.lists[0]);
      this.user_name = localStorage.getItem("username");
    }
    else {
      this.router.navigate(['/homepage']);
    }
  }

}
