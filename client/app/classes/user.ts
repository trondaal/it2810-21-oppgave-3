// The user class is used to represent a user
export class User {
  name: string;
  email: string;
  password: string;
  token: string;
}
