// The movie class is used to represent a movie
export class Movie {
  id: number;
  title: string;
  year: number;
  rank: number;
  description: string;
  poster: string;
  backdrop: string;

  static fromTmdb(tmdbMovie) {
    const movie = new Movie();
    movie.title = tmdbMovie.title;
    movie.id = tmdbMovie.id;
    movie.rank = tmdbMovie.vote_average;
    movie.description = tmdbMovie.overview;
    movie.poster = `https://image.tmdb.org/t/p/original${tmdbMovie.poster_path}`;
    movie.backdrop = `https://image.tmdb.org/t/p/original${tmdbMovie.backdrop_path}`;
    return movie;
  }
}
