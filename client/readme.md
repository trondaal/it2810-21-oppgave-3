# Planflix server

### Dependencies
To get started the only requirement is that you have ````node (v7.0.0)```` and ````npm```` in your path.

### Install
To install the libraries the project requires, type this command in the root directory.

### Configuration
All configurations reside in the ````conf```` directory.

````
npm install
````

### Start
To start the webserver on localhost port ````8000````, type this command in the root directory.

````
npm start
````

## Architecture
All the functionality of the web-application resides in the /app folder, except for some minor styling in the styles.css file.

### App folder
The app folder contains the following folders:

* classes
* components
* img
* services
* styles
* templates

It also contains the app.component, app.module and main.ts, which is the root of our app.

#### App.component ####
The app.component is our base component, which hosts the navbar component and the main window.

#### App.module ####
This is our module which acts as the engine for the whole application.
It declares all the components, services, and other imports.

#### classes ####
This folder contains the classes used by the different components.
The classes is only used for data representation in the components.

#### components ####
The components folder contains all the different components used by the app.

* homepage
: The homepage component shows the user a welcome message, as well as provide a list of popular movies.

* movie-detail
: The movie detail component shows the user the details about one movie, and is accessed by selecting one movie from a list.

* movie-list
: The movie list component is for rendering a certain list of movies, wether it is from a search or on your profile.

* movie-search
: The movie search component handles the search results, and lets you sort or filter the result

* navbar
: The navbar component is the most important component of all. It is always displayed at the top, and lets the user navigate the app, log in or out, register, and enter a search.

* profile
: The profile component lets you access your lists and search history.

#### img ####
The image folder contains the .svg files used by the application.

#### services ####
The services folder contains the services used by the components. The services connects to the database and fetches or pushes some data from or to the database.

* http.service
: The http service handles the user-token when communicating with the database.

* movie.service
: The movie service fetches movies from the database.

* user.service
: The user service handles all the actions regarding the user, such as logging in, registering, fetching lists, adding to lists etc.

#### styles ####
The styles folder contains all the css for all the different components.

#### templates ####
The templates folder contains all the HTML templates for the different components.
