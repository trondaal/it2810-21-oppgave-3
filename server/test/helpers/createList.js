const User = require('src/user/user');
const List = require('src/list/list');
const ListRepository = require('src/list/listRepository');
const SqliteDatabase = require('src/database/sqliteDatabase');

function createList(username, name) {
  const database = new SqliteDatabase(':memory:');
  const listRepository = new ListRepository(database);
  const user = new User(username);
  const list = new List(name);
  return listRepository.create(user, list);
}

module.exports = createList;
