const User = require('src/user/user');
const ListRepository = require('src/list/listRepository');
const SqliteDatabase = require('src/database/sqliteDatabase');

function addMovieToList(username, listId, movieId) {
  const database = new SqliteDatabase(':memory:');
  const listRepository = new ListRepository(database);
  const user = new User(username);
  return listRepository.addMovie(user, listId, movieId);
}

module.exports = addMovieToList;
