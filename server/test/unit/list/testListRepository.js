const assert = require('assert');
const List = require('src/list/list');
const User = require('src/user/user');
const ListRepository = require('src/list/listRepository');
const SqliteDatabase = require('src/database/sqliteDatabase');

describe('ListRepository', () => {
  beforeEach(() => {
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  beforeEach(() => {
    this.insertUser = (username, email, password) =>
      this.database.run('insert into user (user_name, email, password) values (?, ?, ?)', [username, email, password]);

    this.insertList = (name, owner) =>
      this.database.run('insert into list (name, user_name) values (?, ?)', [name, owner]);

    this.insertListContent = (listId, movieId) =>
      this.database.run('insert into list_content (list_id, movie_id) values (?, ?)', [listId, movieId]);
  });

  describe('allByUser', () => {
    it('should return all the lists owned by a user', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.allByUser(user))
      .then(lists => assert.equal(lists.length, 1));
    });
  });

  describe('remove', () => {
    it('should succeed if list exists', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.remove(user, 1))
      .then(() => this.database.get('select count(*) as count from list'))
      .then(row => assert.equal(row.count, 0));
    });

    it('should fail if list does not exist', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => listRepository.remove(user, 1))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Could not delete list with id 1.'));
    });

    it('should succeed even if list contains movies', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.remove(user, 1))
      .then(() => this.database.get('select count(*) as count from list'))
      .then(row => assert.equal(row.count, 0));
    });
  });

  describe('create', () => {
    it('should create a list for a user', () => {
      const listRepository = new ListRepository(this.database);
      const spaceMovies = new List('Space movies');
      const aage = new User('Aage');

      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => listRepository.create(aage, spaceMovies))
      .then(() => this.database.get('select count(*) as count from list where user_name = "Aage"'))
      .then(row => assert.equal(row.count, 1));
    });

    it('should not create a list for a user if user does not exist in the database', () => {
      const listRepository = new ListRepository(this.database);
      const spaceMovies = new List('Space movies');
      const aage = new User('Aage');

      return Promise.resolve()
      .then(() => listRepository.create(aage, spaceMovies))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'User does not exist.'));
    });

    it('should raise an exception if the list name is empty', () => {
      const listRepository = new ListRepository(this.database);
      const spaceMovies = new List('');
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => listRepository.create(aage, spaceMovies))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The name of a list can not be empty.'));
    });

    it('should create a list with the correct name', () => {
      const listRepository = new ListRepository(this.database);
      const romanticMovies = new List('Romantic movies');
      const aage = new User('Aage');

      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => listRepository.create(aage, romanticMovies))
      .then(() => this.database.get('select name from list'))
      .then(row => assert.equal(row.name, 'Romantic movies'));
    });
  });

  describe('addMovie', () => {
    it('should create list_content with correct list_id and movie_id', () => {
      const listRepository = new ListRepository(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.addMovie(aage, 1, 1))
      .then(() => this.database.get('select list_id, movie_id from list_content'))
      .then(row => assert.deepEqual(row, { list_id: 1, movie_id: 1 }));
    });

    it('should raise an exception if the user does not own the list', () => {
      const listRepository = new ListRepository(this.database);
      const michael = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.addMovie(michael, 1, 1))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'You could not add movie to that list.'));
    });

    it('should raise an exception if list does not exist', () => {
      const listRepository = new ListRepository(this.database);
      const michael = new User('Michael');
      return Promise.resolve()
      .then(() => listRepository.addMovie(michael, 1, 1))
      .then(() => this.database.get('select list_id, movie_id from list_content'))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'You could not add movie to that list.'));
    });

    it('should fail if the movie is already in the list', () => {
      const listRepository = new ListRepository(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.addMovie(aage, 1, 1))
      .then(() => listRepository.addMovie(aage, 1, 1))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'That movie is already in this list.'));
    });
  });

  describe('readMoviesInList', () => {
    it('should raise an exception if the user does not own the list', () => {
      const listRepository = new ListRepository(this.database);
      const michael = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Aage'))
      .then(() => listRepository.readMoviesInList(michael, 1))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'You can not see the movies in that list.'));
    });
  });

  describe('removeMovie', () => {
    it('should remove list_content with correct list_id and movie_id', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.removeMovie(user, 1, 1))
      .then(() => this.database.get('select count(*) as count from list_content'))
      .then(row => assert.equal(row.count, 0));
    });

    it('should raise an exception if movie is not in the list', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.removeMovie(user, 1, 0))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'Could not delete list or movie with that id.'));
    });

    it('should raise an exception if the user does not own the list', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => listRepository.removeMovie(user, 1, 1))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'You can not remove a movie from that list.'));
    });

    it('should raise an exception if list does not exist', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.removeMovie(user, 0, 1))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'You can not remove a movie from that list.'));
    });
  });

  describe('read', () => {
    it('should fail if list was not found given id', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Aage');
      return Promise.resolve()
      .then(() => listRepository.read(user, 1))
      .then(assert.fail)
      .catch(error => assert.equal(error.message, 'Could not find list with id 1.'));
    });

    it('should succeed if list was found given id', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(list.name, 'Space movies'));
    });

    it('should return list as an instance of List', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => listRepository.read(user, 1))
      .then(list => assert(list instanceof List));
    });

    it('should return the second list if the second id is read', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertList('Romantic movies', 'Michael'))
      .then(() => listRepository.read(user, 2))
      .then(list => assert.equal(list.name, 'Romantic movies'));
    });

    it('should return a list containing 1 movie if it has one', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(list.length, 1));
    });

    it('should return a list containing 1 movie as a Number instance if it has one', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(typeof list.get(0), 'number'));
    });

    it('should return a list containing 0 movies if it has none', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(list.length, 0));
    });

    it('should return a list containing 2 movies if it has two', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => this.insertListContent(1, 2))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(list.length, 2));
    });

    it('should return a list with 1 movie with the correct id', () => {
      const listRepository = new ListRepository(this.database);
      const user = new User('Michael');
      return Promise.resolve()
      .then(() => this.insertUser('Michael', 'mike@gmail.com', 'password'))
      .then(() => this.insertList('Space movies', 'Michael'))
      .then(() => this.insertListContent(1, 1))
      .then(() => listRepository.read(user, 1))
      .then(list => assert.equal(list.get(0), 1));
    });
  });
});
