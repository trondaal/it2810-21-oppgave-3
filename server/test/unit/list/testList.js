const assert = require('assert');
const List = require('src/list/list');
const Movie = require('src/movie/movie');

describe('List', () => {
  beforeEach(() => {
    this.interstellar = new Movie();
    this.interstellar.title = 'Interstellar';
    this.interstellar.rank = 8.2;

    this.fightClub = new Movie();
    this.fightClub.title = 'Fight Club';
    this.fightClub.rank = 9.2;
  });

  it('should have length of 0 if no movies have been added', () => {
    const spaceMovies = new List('Space movies');
    assert.equal(spaceMovies.length, 0);
  });

  it('should have length of 1 if one movie has been added', () => {
    const spaceMovies = new List('Space movies');
    spaceMovies.add(this.interstellar);
    assert.equal(spaceMovies.length, 1);
  });
});
