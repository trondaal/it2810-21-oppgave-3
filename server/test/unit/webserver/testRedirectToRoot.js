const assert = require('assert');
const request = require('supertest-as-promised');
const app = require('src/webserver/app');

describe('Redirect to root', () => {
  it('should return 302 redirect to the root on wrong URL', () =>
    request(app)
    .get('/api/v1/does/not/exist')
    .expect(302)
    .then(response => assert.equal(response.header.location, '/'))
  );
});
