const assert = require('assert');
const User = require('src/user/user');
const SearchHistory = require('src/searchHistory/searchHistory');
const SqliteDatabase = require('src/database/sqliteDatabase');

describe('SearchHistory', () => {
  beforeEach(() => {
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  beforeEach(() => {
    this.insertUser = (username, email, password) =>
      this.database.run('insert into user (user_name, email, password) values (?, ?, ?)', [username, email, password]);

    this.insertSearchHistory = (query, username) =>
      this.database.run('insert into search_history (search_query, user_name) values (?, ?)', [query, username]);
  });

  describe('create', () => {
    it('should create a search history entry', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => this.database.get('select * from search_history'))
      .then(row => assert.deepEqual(row, { id: 1, search_query: 'interstellar', user_name: 'Aage' }));
    });

    it('should create two identical search history entries', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => this.database.get('select count(*) as count from search_history'))
      .then(row => assert.equal(row.count, 2));
    });

    it('should not create a search entry user if user does not exist in the database', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Could not create search history.'));
    });

    xit('should not create a duplicate search entry and succeed', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => searchHistory.create(aage, 'interstellar'))
      .then(() => this.database.get('select count(*) as count from search_history'))
      .then(row => assert.equal(row.count, 1));
    });
  });

  describe('read', () => {
    it('should retrieve a single search query for a user', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertSearchHistory('interstellar', 'Aage'))
      .then(() => searchHistory.read(aage))
      .then(searchQueries => assert.deepEqual(searchQueries, ['interstellar']));
    });

    it('should retrieve several search queries for a user', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertSearchHistory('interstellar', 'Aage'))
      .then(() => this.insertSearchHistory('mary poppins', 'Aage'))
      .then(() => searchHistory.read(aage))
      .then(searchQueries => assert(searchQueries.includes('interstellar') && searchQueries.includes('mary poppins')));
    });

    it('should retrieve latest search queries first', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => this.insertSearchHistory('interstellar', 'Aage'))
      .then(() => this.insertSearchHistory('mary poppins', 'Aage'))
      .then(() => this.insertSearchHistory('aaron poppins', 'Aage'))
      .then(() => searchHistory.read(aage))
      .then(searchQueries =>
        assert.equal(searchQueries[0], 'aaron poppins') &&
        assert.equal(searchQueries[1], 'mary poppins') &&
        assert.equal(searchQueries[2], 'interstellar')
      );
    });

    it('should retrieve no search queries for a user that has not searched', () => {
      const searchHistory = new SearchHistory(this.database);
      const aage = new User('Aage');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => searchHistory.read(aage))
      .then(searchQueries => assert.deepEqual(searchQueries, []));
    });
  });
});
