const assert = require('assert');
const User = require('src/user/user');
const UserSecurity = require('src/user/userSecurity');
const SqliteDatabase = require('src/database/sqliteDatabase');

describe('UserSecurity', () => {
  beforeEach(() => {
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  describe('createNewUser', () => {
    it('should create a new user with a username and an email', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'ping@something.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, 'password'))
      .then(() => this.database.get('select email, user_name from user'))
      .then(row => assert.deepEqual(row, { email: 'ping@something.com', user_name: 'KingPing' }));
    });

    it('should create three lists for the new user', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'ping@something.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, 'password'))
      .then(() => this.database.all('select name from list where user_name = "KingPing"'))
      .then(rows => assert.equal(rows.length, 3));
    });

    it('should not create a new user with an empty username', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('', 'ping@something.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, 'password'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The username is empty.'));
    });

    it('should not create a new user with an identical username with different casing', () => {
      const userSecurity = new UserSecurity(this.database);
      const mike = new User('Mike', 'mike@gmail.com');
      const MIKE = new User('MIKE', 'mike@hotmail.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(mike, 'password'))
      .then(() => userSecurity.createNewUser(MIKE, 'password'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'That username is already taken'));
    });

    it('should not create a new user with an empty email', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', '');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, 'password'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The email is empty.'));
    });

    it('should not create a new user with an empty password', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'king@gmail.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, ''))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The password is empty.'));
    });

    it('should not create a new user with a username consisting of only whitespace', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('           ', 'ping@something.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The username is empty.'));
    });

    it('should not create a new user with an email consisting of only whitespace', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', '             ');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The email is empty.'));
    });

    it('should not store the clear text password in the database', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'ping@gmail.com');
      return Promise.resolve()
      .then(() => userSecurity.createNewUser(ping, 'cleartext password'))
      .then(() => this.database.get('select password from user'))
      .then(row => assert.notEqual(row.password, 'cleartext password'));
    });
  });

  describe('login', () => {
    it('should return true if correct credentials are provided', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'ping@gmail.com');
      return Promise.resolve()
      .then(() => this.database.run(
        'INSERT INTO user (user_name, email, password) ' +
        'VALUES ("KingPing", "ping@gmail.com", "$2a$10$XdcWXDNFZl2rFG.X9z3uIuvWC404jQcC5dx5.lsUJpaSoZiQN1FBq")'
      ))
      .then(() => userSecurity.login(ping, 'password'));
    });

    it('should reject if incorrect credentials are provided', () => {
      const userSecurity = new UserSecurity(this.database);
      const ping = new User('KingPing', 'ping@gmail.com');
      return Promise.resolve()
      .then(() => this.database.run(
        'INSERT INTO user (user_name, email, password) ' +
        'VALUES ("KingPing", "ping@gmail.com", "$2a$10$XdcWXDNFZl2rFG.X9z3uIuvWC404jQcC5dx5.lsUJpaSoZiQN1FBq")'
      ))
      .then(() => userSecurity.login(ping, 'anti-password'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Wrong username or password.'));
    });
  });

  describe('hashPassword', () => {
    it('should return something else than the input', () =>
       Promise.resolve()
        .then(() => UserSecurity.hashPassword('1234'))
        .then(hash => assert.equal(typeof hash, 'string'))
    );

    it('should return something else than "hash"', () =>
       Promise.resolve()
        .then(() => UserSecurity.hashPassword('1234'))
        .then(hash => assert.notEqual(hash, 'hash'))
    );
  });
});
