const assert = require('assert');
const User = require('src/user/user');
const Token = require('src/user/token');

describe('Token', () => {
  describe('fromString', () => {
    it('should decode the correct user given a valid token', () => {
      const token = Token.fromString(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
          'eyJ1c2VybmFtZSI6IktpbmdQaW5nIn0.' +
          'rwTDqBUIstLH9BE0gXOHJtgpYQfHtguCwXLGap_Ztk8',
          'secret'
      );
      assert.equal(token.user.username, 'KingPing');
    });

    it('should raise an exception if the encoded token is bad', () => {
      assert.throws(() => {
        Token.fromString(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
          'THIS IS NOT A VALID TOKEN.' +
          'eyJlbWFpbCI6InBpbmdAZ21haWwuY29tIn0.',
          'secret'
        );
      }, /invalid/);
    });

    it('should raise an exception if the token is good but secret is bad', () => {
      assert.throws(() => {
        Token.fromString(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
          'eyJ1c2VybmFtZSI6IktpbmdQaW5nIn0.' +
          'rwTDqBUIstLH9BE0gXOHJtgpYQfHtguCwXLGap_Ztk8',
          'THIS IS THE WRONG SECRET'
        );
      }, /invalid/);
    });

    it('should construct a new Token with user as an instance of User', () => {
      const token = Token.fromString(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
          'eyJ1c2VybmFtZSI6IktpbmdQaW5nIn0.' +
          'rwTDqBUIstLH9BE0gXOHJtgpYQfHtguCwXLGap_Ztk8',
          'secret'
      );
      assert(token.user instanceof User);
    });
  });

  describe('toString', () => {
    it('should generate different tokens for different users', () => {
      const mike = new User('mike@gmail.com');
      const ping = new User('ping@gmail.com');
      const firstToken = new Token(mike);
      const secondToken = new Token(ping);
      assert.notEqual(firstToken.toString('secret'), secondToken.toString('secret'));
    });
  });
});
