const assert = require('assert');
const User = require('src/user/user');

const UserRepository = require('src/user/userRepository');
const SqliteDatabase = require('src/database/sqliteDatabase');

describe('UserRepository', () => {
  beforeEach(() => {
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  beforeEach(() => {
    this.insertUser = (username, email, password) =>
      this.database.run('insert into user (user_name, email, password) values (?, ?, ?)', [username, email, password]);
  });

  describe('create', () => {
    it('should create a user', () => {
      const userRepository = new UserRepository(this.database);
      const ping = new User('KingPing');
      return Promise.resolve()
      .then(() => userRepository.create(ping))
      .then(() => this.database.get('select user_name from user'))
      .then(row => assert.equal(row.user_name, 'KingPing'));
    });

    it('shoulld create a user with an email', () => {
      const userRepository = new UserRepository(this.database);
      const mike = new User('Mike', 'test@email.com');
      return Promise.resolve()
      .then(() => userRepository.create(mike))
      .then(() => this.database.get('select email from user'))
      .then(row => assert.equal(row.email, 'test@email.com'));
    });

    it('should create a user with a different username', () => {
      const userRepository = new UserRepository(this.database);
      const mike = new User('Mike');
      return Promise.resolve()
      .then(() => userRepository.create(mike))
      .then(() => this.database.get('select user_name from user'))
      .then(row => assert.equal(row.user_name, 'Mike'));
    });

    it('should not be able to create a user with an already existing email', () => {
      const userRepository = new UserRepository(this.database);
      const aage = new User('Aage', 'aage@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Aage123', 'aage@gmail.com', 'password'))
      .then(() => userRepository.create(aage))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'That email is already taken'));
    });

    it('should not be able to create a user with an already existing username', () => {
      const userRepository = new UserRepository(this.database);
      const aage = new User('Aage', 'aage@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'theman@gmail.com', 'password'))
      .then(() => userRepository.create(aage))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'That username is already taken'));
    });

    it('should not be able to create a user with an already existing username and email', () => {
      const userRepository = new UserRepository(this.database);
      const aage = new User('Aage', 'aage@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => userRepository.create(aage))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'That username is already taken'));
    });
  });

  describe('changePassword', () => {
    it('should change the password if the user exists', () => {
      const userRepository = new UserRepository(this.database);
      const aage = new User('Aage', 'aage@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Aage', 'aage@gmail.com', 'password'))
      .then(() => userRepository.changePassword(aage, 'blehs'))
      .then(() => this.database.get('select password from user'))
      .then(row => assert.equal(row.password, 'blehs'));
    });

    it('should change the password for another user', () => {
      const userRepository = new UserRepository(this.database);
      const ping = new User('Ping', 'ping@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Ping', 'ping@gmail.com', 'password'))
      .then(() => userRepository.changePassword(ping, 'blehs'))
      .then(() => this.database.get('select password from user'))
      .then(row => assert.equal(row.password, 'blehs'));
    });
  });

  describe('retrieveHash', () => {
    it('should return true if there exists a username with the password', () => {
      const userRepository = new UserRepository(this.database);
      const ping = new User('Ping', 'ping@gmail.com');
      return Promise.resolve()
      .then(() => this.insertUser('Ping', 'ping@gmail.com', 'password'))
      .then(() => userRepository.retrieveHash(ping))
      .then(password => assert.equal(password, 'password'));
    });

    it('should return raise an exception if there is not a username', () => {
      const userRepository = new UserRepository(this.database);
      const ping = new User('Mike', 'test@mordi');
      return Promise.resolve()
      .then(() => this.insertUser('Ping', 'ping@gmail.com', 'password'))
      .then(() => userRepository.retrieveHash(ping))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'The user does not exist.'));
    });
  });
});
