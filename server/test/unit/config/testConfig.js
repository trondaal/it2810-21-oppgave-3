const assert = require('assert');
const Config = require('src/config/configuration.js');

describe('Config', () => {
  before(() => {
    this.backedUpEnvironmentVariables = Object.assign({}, process.env);
    this.backedUpConfigFields = Object.assign({}, Config.getInstance().fields);
  });

  after(() => {
    process.env = this.backedUpEnvironmentVariables;
    Config.getInstance().fields = this.backedUpConfigFields;
  });

  it('should retrieve section and option from config file', () => {
    Config.getInstance().fields = { 'web': { 'port': 1337 } };
    assert.equal(Config.getInstance().get('web', 'port'), 1337);
  });

  it('should resolve to environment variables if option is filtered with env', () => {
    Config.getInstance().fields = { 'web': { 'port': 'env|PLANFLIX_WEBSERVER_PORT' } };
    process.env.PLANFLIX_WEBSERVER_PORT = '1338';
    assert.equal(Config.getInstance().get('web', 'port'), 1338);
  });

  it('raises an exception if the environment variable does not exist', () => {
    Config.getInstance().fields = { 'web': { 'port': 'env|DOES_NOT_EXIST' } };
    process.env.PLANFLIX_WEBSERVER_PORT = '1338';
    assert.throws(() => Config.getInstance().get('web', 'port'), /tom/);
  });

  it('raises an exception if the environment variable is empty', () => {
    Config.getInstance().fields = { 'web': { 'port': 'env|PLANFLIX_WEBSERVER_PORT' } };
    process.env.PLANFLIX_WEBSERVER_PORT = '';
    assert.throws(() => Config.getInstance().get('web', 'port'), /tom/);
  });

  it('raises an exception if the section does not exist in the file', () => {
    Config.getInstance().fields = { 'web': { 'port': '1338' } };
    assert.throws(() => Config.getInstance().get('woops', 'port'), /felt/i);
  });

  it('raises an exception if the option does not exist in the file', () => {
    Config.getInstance().fields = { 'web': { 'port': '1338' } };
    assert.throws(() => Config.getInstance().get('web', 'woops'), /felt/i);
  });

  it('returns an array if field is an array', () => {
    Config.getInstance().fields = { 'bouncer': { 'whitelist': [1, 2, 3] } };
    assert.deepEqual(Config.getInstance().get('bouncer', 'whitelist'), [1, 2, 3]);
  });

  it('decodes field as base64 if base64| is before the variable', () => {
    Config.getInstance().fields = { 'web': { 'port': 'base64|MTMzOA==' } };
    assert.equal(Config.getInstance().get('web', 'port'), 1338);
  });

  it('decodes environment variable as base64 if BASE64= is before the variable', () => {
    Config.getInstance().fields = { 'web': { 'port': 'env|base64|PLANFLIX_WEBSERVER_PORT' } };
    process.env.PLANFLIX_WEBSERVER_PORT = 'MTMzOA==';
    assert.equal(Config.getInstance().get('web', 'port'), 1338);
  });
});
