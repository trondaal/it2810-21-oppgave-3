const assert = require('assert');
const Movie = require('src/movie/movie');
const TMDB = require('src/tmdb/tmdb');
const Cache = require('src/tmdb/cache');
const SqliteDatabase = require('src/database/sqliteDatabase');
const tmdbMock = require('test/helpers/tmdbMock');

const emptyQuerySuccess = require('test/fixtures/empty-query-success-response.json');
const interstellarQuerySuccess = require('test/fixtures/interstellar-query-success-response.json');
const interstellarInfoSuccess = require('test/fixtures/interstellar-info-success-response.json');
const popularMoviesSuccessResponse = require('test/fixtures/popular-movies-success-response.json');

describe('TMDB', function test() {
  beforeEach(() => {
    this.mockMoviedb = tmdbMock();
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  describe('popular', () => {
    it('should return the "Doctor Strange" year in the collection of popular movies', () => {
      this.mockMoviedb.response = popularMoviesSuccessResponse;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(movies =>
        assert.equal(movies[0].year, 2016)
      );
    });

    it('should return the "Doctor Strange" overview in the collection of popular movies', () => {
      this.mockMoviedb.response = popularMoviesSuccessResponse;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(movies =>
        assert(movies[0].overview.startsWith('After his career is destroyed'))
      );
    });

    it('should return "Doctor Strange" as the most popular movie if themoviedb.org', () => {
      this.mockMoviedb.response = popularMoviesSuccessResponse;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(movies =>
        assert.equal(movies[0].title, 'Doctor Strange')
      );
    });

    it('should return "Insurgent" as the least popular movie if themoviedb.org', () => {
      this.mockMoviedb.response = popularMoviesSuccessResponse;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(movies =>
        assert.equal(movies[19].title, 'Insurgent')
      );
    });

    it('should return error message if themoviedb.org does not find any popular movies', () => {
      this.mockMoviedb.error = new Error('500: Server is not responding');
      this.mockMoviedb.response = null;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not find popular movies.')
      );
    });
  });

  describe('lookup', () => {
    it('should return 1 movie if themoviedb.org finds a movie with given id', () => {
      this.mockMoviedb.response = interstellarInfoSuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      tmdb.lookup(157336)
      .then(movie =>
        assert.equal(movie.title, 'Interstellar')
      );
    });

    it('should return reject Promise if themoviedb.org does not find a movie with given id', () => {
      this.mockMoviedb.error = new Error('404: Not found');
      this.mockMoviedb.response = null;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.lookup(1234)
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not find a movie with that id.')
      );
    });
  });

  describe('search', () => {
    it('should return 8 movies if themoviedb.org finds 8 matches', () => {
      this.mockMoviedb.response = interstellarQuerySuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar')
      .then(movies =>
        assert.equal(movies.length, 8)
      );
    });

    it('should return 0 movies if themoviedb.org finds 0 matches', () => {
      this.mockMoviedb.response = emptyQuerySuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('movie that does not exist')
      .then(movies =>
        assert.equal(movies.length, 0)
      );
    });

    it('should return Interstellar as a Movie if themoviedb.org finds it', () => {
      this.mockMoviedb.response = interstellarQuerySuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar')
      .then(movies =>
        assert(movies[0] instanceof Movie)
      );
    });

    it('should reject the Promise if themoviedb.org is down', () => {
      this.mockMoviedb.error = new Error('Could not connect to themoviedb.org');
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar')
      .then(assert.fail)
      .catch(error =>
        assert(error.message.startsWith('Could not search'))
      );
    });

    it('should reject the Promise if themoviedb.org response can not be parsed', () => {
      this.mockMoviedb.response = 'hwhfhwehhwefhwefjwefwkefkew';
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar')
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not parse results.')
      );
    });
  });
});
