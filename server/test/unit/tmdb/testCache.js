const assert = require('assert');
const Cache = require('src/tmdb/cache');
const SqliteDatabase = require('src/database/sqliteDatabase');

describe('Cache', () => {
  beforeEach(() => {
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  beforeEach(() => {
    this.insertCacheEntry = (key, value, timeToLive) =>
      this.database.run('insert into cache (key, value, time_to_live) values (?, ?, ?)', [key, value, timeToLive]);
  });

  describe('set', () => {
    it('should create an entry in the cache with the correct key', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => cache.set('foo', 'bar'))
      .then(() => this.database.get('select key from cache'))
      .then(row => assert.equal(row.key, 'foo'));
    });

    it('should create an entry in the cache with the correct value', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => cache.set('foo', { 'bar': 'value' }))
      .then(() => this.database.get('select value from cache'))
      .then(row => assert.equal(row.value, '{"bar":"value"}'));
    });

    it('should not blow up given a large value', () => {
      const cache = new Cache(this.database);
      const largeValue = new Array(12000).join('.');
      return Promise.resolve()
      .then(() => cache.set('foo', largeValue))
      .then(() => this.database.get('select value from cache'))
      .then(row => assert.equal(row.value, `"${largeValue}"`));
    });

    it('should create an entry with a default time to live of 1 minute', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => cache.set('foo', 'bar'))
      .then(() => this.database.get('select time_to_live from cache'))
      .then(row => assert.equal(row.time_to_live, 60));
    });

    it('should create an entry with a timestamp that is set to now', () => {
      const cache = new Cache(this.database);
      const now = new Date();
      return Promise.resolve()
      .then(() => cache.set('foo', 'bar'))
      .then(() => this.database.get('select created_at from cache'))
      .then(row => assert.equal(new Date(row.created_at).toString(), now.toString()));
    });

    it('should overwrite an old entry', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => cache.set('foo', 'old-bar', 60))
      .then(() => cache.set('foo', 'new-bar', 60))
      .then(() => this.database.get('select value from cache'))
      .then(row => assert.equal(row.value, '"new-bar"'));
    });
  });

  describe('get', () => {
    it('should return the cache entry with the corresponding key', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => this.insertCacheEntry('foo', '{"bar": "value"}', 10))
      .then(() => cache.get('foo'))
      .then(value => assert.deepEqual(value, { bar: 'value' }));
    });

    it('should not return cache entry if no key matches', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => cache.get('foo'))
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not find cache entry with that key.')
      );
    });

    it('should not return an entry if the time to live has expired (negative ttl)', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => this.insertCacheEntry('foo', '{"bar": "value"}', -1))
      .then(() => cache.get('foo'))
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not find cache entry with that key.')
      );
    });

    it('should not return an entry if the time to live has expired (postive ttl)', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => this.insertCacheEntry('foo', '{"bar": "value"}', 1))
      .then(() => new Promise(resolve => setTimeout(resolve, 1000)))
      .then(() => cache.get('foo'))
      .then(assert.fail)
      .catch(error =>
        assert.equal(error.message, 'Could not find cache entry with that key.')
      );
    });

    it('should return an entry if the time to live has not expired', () => {
      const cache = new Cache(this.database);
      return Promise.resolve()
      .then(() => this.insertCacheEntry('foo', '{"bar": "value"}', 10))
      .then(() => cache.get('foo'))
      .then(value => assert.deepEqual(value, { bar: 'value' }));
    });
  });
});
