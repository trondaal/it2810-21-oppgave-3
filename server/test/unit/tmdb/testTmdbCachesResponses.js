const assert = require('assert');
const TMDB = require('src/tmdb/tmdb');
const Cache = require('src/tmdb/cache');
const SqliteDatabase = require('src/database/sqliteDatabase');
const tmdbMock = require('test/helpers/tmdbMock');

const popularMoviesSuccess = require('test/fixtures/popular-movies-success-response.json');
const interstellarInfoSuccess = require('test/fixtures/interstellar-info-success-response.json');
const interstellarQuerySuccess = require('test/fixtures/interstellar-query-success-response.json');

describe('TMDB cache', function test() {
  beforeEach(() => {
    this.mockMoviedb = tmdbMock();
    this.database = new SqliteDatabase(':memory:');
    return Promise.resolve()
    .then(() => this.database.connect())
    .then(() => this.database.tearDown())
    .then(() => this.database.setUp());
  });

  describe('search caching', () => {
    it('should cache search results from TMDB if it succeeded', () => {
      this.mockMoviedb.response = interstellarQuerySuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar', 1, true)
      .then(() => cache.get('s:1:true:interstellar'))
      .then(cacheEntries => assert.deepEqual(cacheEntries, interstellarQuerySuccess));
    });

    it('should cache search results from TMDB as adult true if not supplied', () => {
      this.mockMoviedb.response = interstellarQuerySuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar', 1)
      .then(() => cache.get('s:1:true:interstellar'))
      .then(cacheEntries => assert.deepEqual(cacheEntries, interstellarQuerySuccess));
    });

    it('should not cache search results from TMDB if it fails', () => {
      this.mockMoviedb.error = new Error('404: Not found');
      this.mockMoviedb.response = null;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.search('interstellar', 1, true)
      .catch(() => null)
      .then(() => cache.get('s:1:true:interstellar'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Could not find cache entry with that key.'));
    });

    it('should return the response from the cache if its in the cache', () => {
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return Promise.resolve()
      .then(() => cache.set('s:1:false:interstellar', interstellarQuerySuccess))
      .then(() => tmdb.search('interstellar', 1, false))
      .then(movies => assert.deepEqual(movies[0].title, 'Interstellar'));
    });
  });

  describe('lookup caching', () => {
    it('should cache lookup results from TMDB if it succeeded', () => {
      this.mockMoviedb.response = interstellarInfoSuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.lookup(157336)
      .then(() => cache.get('l:157336'))
      .then(cacheEntries => assert.deepEqual(cacheEntries, interstellarInfoSuccess));
    });

    it('should not cache lookup results from TMDB if it fails', () => {
      this.mockMoviedb.error = new Error('404: Not found');
      this.mockMoviedb.response = null;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.lookup(157336)
      .catch(() => null)
      .then(() => cache.get('l:157336'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Could not find cache entry with that key.'));
    });

    it('should return the response from the cache if its in the cache', () => {
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return Promise.resolve()
      .then(() => cache.set('l:157336', interstellarInfoSuccess))
      .then(() => tmdb.lookup(157336))
      .then(movie => assert.equal(movie.title, 'Interstellar'));
    });
  });

  describe('popular caching', () => {
    it('should cache popular results from TMDB if it succeeded', () => {
      this.mockMoviedb.response = popularMoviesSuccess;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .then(() => cache.get('p:'))
      .then(cacheEntries => assert.deepEqual(cacheEntries, popularMoviesSuccess));
    });

    it('should not cache search results from TMDB if it fails', () => {
      this.mockMoviedb.error = new Error('404: Not found');
      this.mockMoviedb.response = null;
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return tmdb.popular()
      .catch(() => null)
      .then(() => cache.get('p:'))
      .then(() => assert.fail())
      .catch(error => assert.equal(error.message, 'Could not find cache entry with that key.'));
    });

    it('should return the response from the cache if its in the cache', () => {
      const cache = new Cache(this.database);
      const tmdb = new TMDB(cache, 'bogus-api-key', this.mockMoviedb);
      return Promise.resolve()
      .then(() => cache.set('p:', popularMoviesSuccess))
      .then(() => tmdb.popular())
      .then(movies => assert.deepEqual(movies[0].title, 'Doctor Strange'));
    });
  });
});
