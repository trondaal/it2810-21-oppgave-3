const assert = require('assert');
const convertTmdbMovieToPlanFlixMovie = require('src/tmdb/convertTmdbMovieToPlanFlixMovie');

describe('Convert TMDB movie to PlanFlix Movie', () => {
  beforeEach(() => {
    this.interstellarTmdbMovie = {
      id: 157336,
      title: 'Interstellar',
      vote_average: 8.1,
      poster_path: '/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
      backdrop_path: '/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg',
      overview:
            'Interstellar chronicles the adventures of a group of explorers who make ' +
            'use of a newly discovered wormhole to surpass the limitations on human ' +
            'space travel and conquer the vast distances involved in an this.interstellar voyage.',
      adult: false,
      release_date: '2014-11-05',
      genre_ids: [],
      original_title: 'Interstellar',
      original_language: 'en',
      popularity: 15.414358,
      vote_count: 5850,
      video: false,
    };
  });

  it('should translate the TMDB release date to movie year', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.strictEqual(interstellar.year, 2014);
  });

  it('should translate the TMDB release date null if it is undefined', () => {
    this.interstellarTmdbMovie.release_date = undefined;
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.strictEqual(interstellar.release_date, null);
  });

  it('should translate the TMDB release date to an instance of Date', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert(interstellar.release_date instanceof Date);
  });

  it('should translate the TMDB release date to an instance of Date with the correct release date', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.release_date.getFullYear(), 2014);
    assert.equal(interstellar.release_date.getMonth(), 10);
    assert.equal(interstellar.release_date.getDate(), 5);
  });

  it('should translate the TMDB release date to undefined to null', () => {
    this.interstellarTmdbMovie.release_date = undefined;
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.strictEqual(interstellar.year, null);
  });

  it('should translate the TMDB title to title', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.title, 'Interstellar');
  });

  it('should translate the TMDB poster_path to a full url for the poster', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.poster, 'https://image.tmdb.org/t/p/w600/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg');
  });

  it('should translate the TMDB poster_path from undefined to null', () => {
    this.interstellarTmdbMovie.poster_path = undefined;
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.poster, null);
  });

  it('should translate the TMDB backdrop_path to a full url for the backdrop', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.backdrop, 'https://image.tmdb.org/t/p/w1920/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg');
  });

  it('should translate the TMDB backdrop_path from undefined to null', () => {
    this.interstellarTmdbMovie.backdrop_path = undefined;
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.backdrop, null);
  });

  it('should translate the TMDB vote_average to rank', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.rank, 8.1);
  });

  it('should translate the TMDB overview to overview', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert(interstellar.overview.startsWith('Interstellar chronicles the adventures'));
  });

  it('should translate the TMDB id to id', () => {
    const interstellar = convertTmdbMovieToPlanFlixMovie(this.interstellarTmdbMovie);
    assert.equal(interstellar.id, 157336);
  });
});
