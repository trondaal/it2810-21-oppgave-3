const assert = require('assert');
const request = require('supertest-as-promised');
const createUser = require('test/helpers/createUser');
const createToken = require('test/helpers/createToken');
const createCacheEntry = require('test/helpers/createCacheEntry');
const resetDatabase = require('test/helpers/resetDatabase');
const interstellarQuerySuccess = require('test/fixtures/interstellar-query-success-response.json');
const app = require('src/webserver/app');

describe('As a user I want to see all my previous searches', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));
  before(() => createCacheEntry('s:1:true:interstellar', interstellarQuerySuccess));

  it('should return 200 with the search results', () =>
    request(app)
    .get('/api/v1/movie/search?query=interstellar&page=1')
    .set('Authorization', createToken('Aage', 'secret'))
    .expect(200)
    .then(() =>

    request(app)
    .get('/api/v1/user/history')
    .set('Authorization', createToken('Aage', 'secret'))
    .expect(200)
    .then(response => assert.deepEqual(response.body.searchQueries, ['interstellar']))
    )
  );
});
