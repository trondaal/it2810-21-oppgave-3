const resetDatabase = require('test/helpers/resetDatabase');
const app = require('src/webserver/app');
const request = require('supertest-as-promised');
const createUser = require('test/helpers/createUser');
const createToken = require('test/helpers/createToken');
const createList = require('test/helpers/createList');

describe('As a user I want to add a movie to a list', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));
  before(() => createList('Aage', 'Romantic comedies'));

  it('should return 200 when list gets created', () =>
    request(app)
    .post('/api/v1/list/1')
    .set('Authorization', createToken('Aage', 'secret'))
    .send({ movieId: 1 })
    .expect(200)
  );
});
