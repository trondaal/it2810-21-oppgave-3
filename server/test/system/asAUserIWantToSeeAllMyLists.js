const assert = require('assert');
const request = require('supertest-as-promised');
const createToken = require('test/helpers/createToken');
const createList = require('test/helpers/createList');
const resetDatabase = require('test/helpers/resetDatabase');
const createUser = require('test/helpers/createUser');
const app = require('src/webserver/app');

describe('As a user I want to see all my lists', () => {
  before(() => resetDatabase());
  before(() => createUser('Ping', 'ping@gmail.com', 'password'));

  it('should return 200 with an array of the lists', () =>

    Promise.resolve()
    .then(() => createList('Ping', 'Space movies'))
    .then(() => createList('Ping', 'Romantic comedies'))

    .then(() =>
      request(app)
      .get('/api/v1/list')
      .set('Authorization', createToken('Ping', 'secret'))
      .expect(200)
      .then(response =>
        assert.equal(response.body[0].name, 'Favorites') &&
        assert.equal(response.body[1].name, 'Have watched') &&
        assert.equal(response.body[2].name, 'Plan to watch') &&
        assert.equal(response.body[3].name, 'Space movies') &&
        assert.equal(response.body[4].name, 'Romantic comedies')
      )
    )
  );
});
