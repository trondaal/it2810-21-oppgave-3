/* eslint-disable no-return-assign */
const net = require('net');

xdescribe('As a developer I want the server to start', () => {
  beforeEach(() =>
    this.server = require('src/webserver/server'));

  it('should listen on port 3000', (done) => {
    net.createConnection(3000, done);
  });

  afterEach(() =>
    this.server.close());
});
