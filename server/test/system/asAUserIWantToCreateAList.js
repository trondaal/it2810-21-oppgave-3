const createUser = require('test/helpers/createUser');
const createToken = require('test/helpers/createToken');
const resetDatabase = require('test/helpers/resetDatabase');
const app = require('src/webserver/app');
const request = require('supertest-as-promised');

describe('As a user I want to create a list', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));

  it('should return 200 when list gets created', () =>
    request(app)
    .post('/api/v1/list')
    .set('Authorization', createToken('Aage', 'secret'))
    .send({ name: 'Space movies' })
    .expect(200)
  );
});
