const assert = require('assert');
const request = require('supertest-as-promised');
const resetDatabase = require('test/helpers/resetDatabase');
const app = require('src/webserver/app');

const createUser = require('test/helpers/createUser');
const createList = require('test/helpers/createList');
const createToken = require('test/helpers/createToken');
const addMovieToList = require('test/helpers/addMovieToList');
const createCacheEntry = require('test/helpers/createCacheEntry');

const interstellarInfoSuccess = require('test/fixtures/interstellar-info-success-response.json');

describe('As a user I want to see the movies in a list', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));
  before(() => createList('Aage', 'Romantic comedies'));
  before(() => addMovieToList('Aage', 1, 157336));
  before(() => addMovieToList('Aage', 1, 157337));
  before(() => createCacheEntry('l:157336', interstellarInfoSuccess));
  before(() => createCacheEntry('l:157337', interstellarInfoSuccess));

  it('should return 200 with an array of the movies', () =>
    request(app)
    .get('/api/v1/list/1')
    .set('authorization', createToken('Aage', 'secret'))
    .expect(200)
    .then(response =>
      assert.equal(response.body[1].title, 'Interstellar')
    )
  );
});
