const resetDatabase = require('test/helpers/resetDatabase');
const app = require('src/webserver/app');
const request = require('supertest-as-promised');
const createUser = require('test/helpers/createUser');
const createToken = require('test/helpers/createToken');
const createList = require('test/helpers/createList');
const addAMovieToList = require('test/helpers/addMovieToList');

describe('As a user I want to remove a movie from a list', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));
  before(() => createList('Aage', 'Romantic comedies'));
  before(() => addAMovieToList('Aage', 1, 2));

  it('should return 200 when list gets created', () =>
    request(app)
    .del('/api/v1/list/1/2')
    .set('Authorization', createToken('Aage', 'secret'))
    .expect(200)
  );
});
