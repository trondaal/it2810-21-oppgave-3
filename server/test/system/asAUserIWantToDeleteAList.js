const createUser = require('test/helpers/createUser');
const createToken = require('test/helpers/createToken');
const createList = require('test/helpers/createList');
const resetDatabase = require('test/helpers/resetDatabase');
const request = require('supertest-as-promised');
const app = require('src/webserver/app');

describe('As a user I want to delete a list', () => {
  before(() => resetDatabase());
  before(() => createUser('Aage', 'aage@gmail.com', 'password'));
  before(() => createList('Aage', 'Space movies'));

  it('should return 200 when list gets deleted', () =>
    request(app)
    .del('/api/v1/list/1')
    .set('Authorization', createToken('Aage', 'secret'))
    .expect(200)
  );
});
