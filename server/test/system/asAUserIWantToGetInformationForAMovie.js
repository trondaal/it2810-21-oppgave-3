const createCacheEntry = require('test/helpers/createCacheEntry');
const resetDatabase = require('test/helpers/resetDatabase');
const request = require('supertest-as-promised');
const app = require('src/webserver/app');
const interstellarInfoSuccess = require('test/fixtures/interstellar-info-success-response.json');

describe('As a user I want to get information for a movie', () => {
  before(() => resetDatabase());
  before(() => createCacheEntry('l:1', interstellarInfoSuccess));

  it('should return 200 with the information', () =>
    request(app)
    .get('/api/v1/movie/1')
    .expect(200)
  );
});
