const documentationFile = require('../docs/file.json');

const descriptions = {};

documentationFile.samples.forEach(sample => {

    const describe = sample.hierarchy.shift();
    const its = sample.hierarchy;
    const name = its[0];

    const endpoint = {
      name: name,
      path: sample.request.path,
      description: describe,
      method: sample.request.method,
      headers: sample.request.headers,
      data: sample.request.data,
      response: sample.response.body,
      responseStatus: sample.response.status
    }

    if (descriptions[endpoint.method + ' ' + endpoint.path] === undefined) {
      descriptions[endpoint.method + ' ' + endpoint.path] = [];
    }
    descriptions[endpoint.method + ' ' + endpoint.path].push(endpoint);
});

function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
}

console.log('# PlanFlix server API\n');
console.log('This document describes all the available HTTP endpoints the server exposes.\n')

Object.keys(descriptions).forEach(methodPath => {
    
    console.log(`## ${methodPath}\n`);

    const its = descriptions[methodPath];
    its.forEach(it => {
        console.log('### ' + capitalizeFirstLetter(it.name));

        console.log("**Method:** `" + it.method + "`\n");

        if (it.data) {
            console.log("**Parameters:**\n");

            console.log('Key           | Value');
            console.log('------------- | -------------');
            Object.keys(it.data).forEach(key => {
              let value = it.data[key];
              console.log(key, ' | ', value);
            });
            console.log("\n\n");
        }

        if (Object.keys(it.headers).length) {
            console.log("**Headers:**\n");

            console.log('Key           | Value');
            console.log('------------- | -------------');
            Object.keys(it.headers).forEach(key => {
              let value = it.headers[key];
              if (key === 'authorization')
                  console.log(key, ' | ', value.substring(0, 50) + ' ...');
              else if (key !== 'content-length')
                  console.log(key, ' | ', value);
            });
        }

        console.log('\n### Response (HTTP status ' + it.responseStatus + ')');

        console.log('````javascript');

        if (Array.isArray(it.response) && it.response.length > 1) {
            console.log(it.response.slice(0, 1)) + ' \n ... ';
        } else {
            console.log(it.response);
        }
        
        console.log('````\n');

    });

    console.log("\n");
    console.log("\n");

});
