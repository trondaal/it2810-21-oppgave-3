#!/usr/bin/env bash
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'List' -f md build src/list/list.js | grep -v "^<" > docs/classes/list.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'ListRepository' -f md build src/list/listRepository.js | grep -v "^<" > docs/classes/listRepository.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'Token' -f md build src/user/token.js | grep -v "^<" > docs/classes/token.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'UserRepository' -f md build src/user/userRepository.js | grep -v "^<" > docs/classes/userRepository.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'UserSecurity' -f md build src/user/userSecurity.js | grep -v "^<" > docs/classes/userSecurity.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'SqliteDatabase' -f md build src/database/sqliteDatabase.js | grep -v "^<" > docs/classes/sqliteDatabase.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'SearchHistory' -f md build src/searchHistory/searchHistory.js | grep -v "^<" > docs/classes/searchHistory.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'TMDB' -f md build src/tmdb/tmdb.js | grep -v "^<" > docs/classes/tmdb.md 
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'Cache' -f md build src/tmdb/cache.js | grep -v "^<" > docs/classes/cache.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'Database' -f md build src/database/database.js | grep -v "^<" > docs/classes/database.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'App' -f md build src/webserver/app.js | grep -v "^<" > docs/classes/app.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'ReadMovieController' -f md build src/webserver/controllers/movie/readMovie.js | grep -v "^<" > docs/controllers/readMovie.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'SearchMoviesController' -f md build src/webserver/controllers/movie/searchMovies.js | grep -v "^<" > docs/controllers/searchMovies.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'PopularMoviesController' -f md build src/webserver/controllers/movie/popularMovies.js | grep -v "^<" > docs/controllers/popularMovies.md

NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'MoviesInListController' -f md build src/webserver/controllers/list/moviesInList.js | grep -v "^<" > docs/controllers/moviesInList.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'AddMovieToListController' -f md build src/webserver/controllers/list/addMovieToList.js | grep -v "^<" > docs/controllers/addMovieToList.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'CreateListController' -f md build src/webserver/controllers/list/createList.js | grep -v "^<" > docs/controllers/createList.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'DeleteListController' -f md build src/webserver/controllers/list/deleteList.js | grep -v "^<" > docs/controllers/deleteList.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'DeleteMovieController' -f md build src/webserver/controllers/list/deleteMovie.js | grep -v "^<" > docs/controllers/deleteMovie.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'ReadListController' -f md build src/webserver/controllers/list/readLists.js | grep -v "^<" > docs/controllers/readLists.md

NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'HistoryController' -f md build src/webserver/controllers/user/history.js | grep -v "^<" > docs/controllers/history.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'LoginController' -f md build src/webserver/controllers/user/login.js | grep -v "^<" > docs/controllers/login.md
NODE_PATH=. node ./node_modules/documentation/bin/documentation.js --name 'RegisterController' -f md build src/webserver/controllers/user/register.js | grep -v "^<" > docs/controllers/register.md
