# Configuration

## Files
This directory contains different config files that are used in different contexts.

Filename                         | Context     | Description
-------------------------------- | ----------- | -------------
````example.json````     | None        | An example of a valid config file. Is automatically copied to ````development.json```` if it does not exist.
````development.json```` | Development | When running ````make serve```` this is the config file that is read. It is ignored by ````git```` so you can safely store secrets.
````test.json````        | Tests       | To avoid having to mock each configuration, this file is used when running ````make test````.

## Fields
A config file can contain multiple sections with multiple fields. A field has a key and a value. 

The value can contain filters. Filters are simply keywords separated by pipes at the start of the value. For instance, ````base64|some-encoded-string```` will decode the string.

````json
{
	"section": {
		"normal-field": "normal-value",
		"base64-encoded-field": "base64|encoded-value",
		"environment-field": "env|ENVIRONMENT_VARIABLE_NAME",
		"base64-encoded-environment-field": "env|base64|ENVIRONMENT_VARIABLE_NAME"
	}
}
````

## Usage
To access fields in the currently loaded configuration you only need to require the Config class.

````json
{
    "webserver": {
        "port": 8000
    }
}
````

To access the port field of the webserver section above, simply use this.

````javascript
const Config = require('config/configuration.js').getInstance();
const port = Config.get('webserver', 'port');
assert.equal(port, 8000);
```` 
