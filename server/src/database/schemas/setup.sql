CREATE TABLE IF NOT EXISTS user (
    email varchar(127) UNIQUE,
    user_name varchar(127) UNIQUE,
    password varchar(127),
    primary key (email, user_name)
);

CREATE TABLE IF NOT EXISTS cache (
    key varchar(255),
    value blob,
    time_to_live INTEGER DEFAULT 60,
    created_at DATE DEFAULT (datetime('now','localtime')),
    primary key(key)
);

CREATE TABLE IF NOT EXISTS search_history (
    id integer,
    user_name varchar(127),
    search_query varchar(255),
    primary key (id),
    foreign key(user_name) REFERENCES user(user_name) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS list (
    list_id integer,
    user_name varchar(127),
    name varchar(127),
    primary key (list_id),
    foreign key (user_name) REFERENCES user(user_name)
);

CREATE TABLE IF NOT EXISTS list_content (
    list_id int,
    movie_id int,
    foreign key(list_id) REFERENCES list(list_id) ON DELETE CASCADE,
    primary key(list_id, movie_id)
);
