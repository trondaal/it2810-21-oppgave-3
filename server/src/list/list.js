class List {

  constructor(name) {
    this.name = name;
    this.movies = [];
  }

  /**
   * Add a movie to the list.
   * @param {Movie} movie the movie you want to add
   * @returns {undefined}
   */
  add(movie) {
    this.movies.push(movie);
  }

  /**
   * Get a movie from the list.
   * @param {Number} index of the movie you want to get
   * @returns {Movie}
   */
  get(index) {
    return this.movies[index];
  }

  /**
   * Get a length of the list.
   * @returns {Number}
   */
  get length() {
    return this.movies.length;
  }
}

module.exports = List;
