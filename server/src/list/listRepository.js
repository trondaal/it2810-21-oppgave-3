const assert = require('assert');
const List = require('src/list/list');
const establishedDatabase = require('src/database/database');

class ListRepository {

  constructor(database) {
    this.database = database || establishedDatabase;
    this.queries = {
      'read': 'SELECT name FROM list WHERE list_id = ?',
      'remove': 'DELETE FROM list WHERE list_id = ? AND user_name = ?',
      'create': 'INSERT INTO list (user_name, name) VALUES (?, ?)',
      'addMovie': 'INSERT INTO list_content (list_id, movie_id) values (?, ?)',
      'readMovies': 'SELECT * from list_content as L WHERE list_id = ?',
      'removeMovie': 'DELETE FROM list_content WHERE list_id = ? AND movie_id = ?',
      'getListByUsername': 'SELECT * FROM list WHERE user_name = ? AND list_id = ?',
      'getListsByUsername': 'SELECT * FROM list WHERE user_name = ?',
    };
  }

  /**
   * Retrieves all the lists for a user.
   * @param {User} user existing user
   * @returns {Promise} succeeds if lists were found
   */
  allByUser(user) {
    return this.database.all(this.queries.getListsByUsername, user.username).then(rows =>
       rows.map((row) => {
         const list = new List(row.name);
         list.id = row.list_id;
         list.movies = undefined;
         return list;
       })
    );
  }

  /**
   * Create a new list in the database.
   * @param {User} user existing user
   * @param {List} list that will be created
   * @returns {Promise} succeeds if list was created
   */
  create(user, list) {
    assert(list.name.trim().length, 'The name of a list can not be empty.');
    return Promise.resolve()
    .then(() => this.database.run(this.queries.create, [user.username, list.name]))
    .catch(() => {
      throw new Error('User does not exist.');
    });
  }

  /**
   * Retrieve a list that belongs to a user.
   * @param {User} user user that owns the list
   * @param {Number} listId for the list you want retrieved
   * @returns {Promise} succeeds if list was found
   */
  read(user, listId) {
    return this.database.get(this.queries.read, listId).then((row) => {
      assert.notEqual(row, undefined, `Could not find list with id ${listId}.`);
      return this.readMoviesInList(user, listId).then((movies) => {
        const list = new List();
        list.name = row.name;
        list.movies = movies;
        return list;
      });
    });
  }

  /**
   * Remove a list that belongs to a user.
   * @param {User} user that owns the list
   * @param {Number} listId for the list you want removed
   * @returns {Promise} succeeds if list was removed
   */
  remove(user, listId) {
    return this.database.run(this.queries.remove, [listId, user.username]).then((result) => {
      assert(result.changes, `Could not delete list with id ${listId}.`);
    });
  }

  /**
   * Retrieve the movies in a list.
   * @param {User} user of the user that owns the list
   * @param {Number} listId for the list you want to get movies out of
   * @returns {Promise} succeeds if list was removed
   */
  readMoviesInList(user, listId) {
    return Promise.resolve()
    .then(() => this.database.get(this.queries.getListByUsername, [user.username, listId]))
    .then(row => assert(row, 'You can not see the movies in that list.'))
    .then(() => this.database.all(this.queries.readMovies, listId))
    .then(rows => rows.map(row => row.movie_id)
    );
  }

  /**
   * Add a movie to a list belonging to a user.
   * @param {User} user of the user that owns the list
   * @param {Number} listId for the list you want to add a movie to
   * @param {Number} movieId for the movie you want to add
   * @returns {Promise} succeeds if list was removed
   */
  addMovie(user, listId, movieId) {
    return Promise.resolve()
    .then(() => this.database.get(this.queries.getListByUsername, [user.username, listId]))
    .then(row => assert(row))
    .then(() => this.database.run(this.queries.addMovie, [listId, movieId]))
    .catch((error) => {
      if (error.message.includes('UNIQUE')) {
        throw new Error('That movie is already in this list.');
      }
      throw new Error('You could not add movie to that list.');
    });
  }

  /**
   * Remove a movie to a list belonging to a user.
   * @param {User} user of the user that owns the list
   * @param {Number} listId for the list you want to remove a movie from
   * @param {Number} movieId for the movie you want to remove
   * @returns {Promise} succeeds if list was removed
   */
  removeMovie(user, listId, movieId) {
    return Promise.resolve()
    .then(() => this.database.get(this.queries.getListByUsername, [user.username, listId]))
    .then(row => assert(row, 'You can not remove a movie from that list.'))
    .then(() => this.database.run(this.queries.removeMovie, [listId, movieId]))
    .then(result => assert(result.changes, 'Could not delete list or movie with that id.'));
  }

}

module.exports = ListRepository;
