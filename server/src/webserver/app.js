const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const tokenToUser = require('./middleware/tokenToUser');
const mustBeAuthenticated = require('./middleware/mustBeAuthenticated');
const clientDirectory = path.join(__dirname, '..', '..', '..', 'client');
const app = express();

/* Decode the request body as JSON */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* Decode the Authorization header if provided */
app.use(tokenToUser);

/* Use the client directory as the root of the webserver */
app.use(express.static(clientDirectory));

/**
 * User
 */
app.post('/api/v1/user', require('./controllers/user/register.js'));
app.post('/api/v1/user/login', require('./controllers/user/login.js'));
app.get('/api/v1/user/history', mustBeAuthenticated, require('./controllers/user/history.js'));

/**
 * Movie
 */
app.get('/api/v1/movie/popular', require('./controllers/movie/popularMovies.js'));
app.get('/api/v1/movie/search', require('./controllers/movie/searchMovies.js'));
app.get('/api/v1/movie/:movieId', require('./controllers/movie/readMovie.js'));

/**
 * List
 */
app.get('/api/v1/list', mustBeAuthenticated, require('./controllers/list/readLists.js'));
app.post('/api/v1/list', mustBeAuthenticated, require('./controllers/list/createList.js'));
app.get('/api/v1/list/:listId', mustBeAuthenticated, require('./controllers/list/moviesInList.js'));
app.post('/api/v1/list/:listId', mustBeAuthenticated, require('./controllers/list/addMovieToList.js'));
app.delete('/api/v1/list/:listId', mustBeAuthenticated, require('./controllers/list/deleteList.js'));
app.delete('/api/v1/list/:listId/:movieId', mustBeAuthenticated, require('./controllers/list/deleteMovie.js'));

/* Redirect route misses to the root of the webserver */
app.use((req, res) => res.redirect('/'));

module.exports = app;
