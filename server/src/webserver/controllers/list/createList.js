const List = require('src/list/list');
const ListRepository = require('src/list/listRepository');
const listRepository = new ListRepository();

/**
 * Controller: Create a list for the logged in user
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function createListController(req, res) {
  const list = new List(req.body.name);
  const user = req.loggedInUser;
  listRepository.create(user, list)
  .then(() => {
    res.send({ success: true });
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = createListController;
