const ListRepository = require('src/list/listRepository');
const listRepository = new ListRepository();

/**
 * Controller: Add a movie to a list.
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function addMovieToListController(req, res) {
  const user = req.loggedInUser;
  const listId = req.params.listId;
  const movieId = req.body.movieId;

  listRepository.addMovie(user, listId, movieId)
  .then(() => {
    res.send({ success: true });
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = addMovieToListController;
