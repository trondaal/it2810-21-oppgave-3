const configuration = require('src/config/configuration').getInstance();
const ListRepository = require('src/list/listRepository');
const Cache = require('src/tmdb/cache');
const TMDB = require('src/tmdb/tmdb');
const listRepository = new ListRepository();
const cache = new Cache();
const tmdb = new TMDB(cache, configuration.get('tmdb', 'apiKey'));

/**
 * Controller: Retireve all the movies in a list belonging to the logged in user
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function moviesInListController(req, res) {
  const user = req.loggedInUser;
  const listId = req.params.listId;

  listRepository.readMoviesInList(user, listId)
  .then((movieIds) => {
    Promise.all(movieIds.map(movieId => tmdb.lookup(movieId)))
    .then((movies) => {
      res.send(movies);
    });
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = moviesInListController;
