const ListRepository = require('src/list/listRepository');
const listRepository = new ListRepository();

/**
 * Controller: Remove a movie from a list belonging to the logged in user
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function deleteMovieController(req, res) {
  const user = req.loggedInUser;
  const listId = req.params.listId;
  const movieId = req.params.movieId;

  listRepository.removeMovie(user, listId, movieId)
  .then(() => {
    res.send({ success: true });
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = deleteMovieController;
