const ListRepository = require('src/list/listRepository');
const listRepository = new ListRepository();

/**
 * Controller: Retireve all lists of the logged in user
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function readListsController(req, res) {
  const user = req.loggedInUser;
  listRepository.allByUser(user)
  .then((lists) => {
    res.send(lists);
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = readListsController;
