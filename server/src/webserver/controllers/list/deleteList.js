const ListRepository = require('src/list/listRepository');
const listRepository = new ListRepository();

/**
 * Controller: Delete a list that belongs to the logged in user
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function deleteListController(req, res) {
  const user = req.loggedInUser;
  const listId = req.params.listId;

  listRepository.remove(user, listId)
  .then(() => {
    res.send({ success: true });
  })
  .catch((error) => {
    res.status(500).send({ success: false, error: error.message });
  });
}

module.exports = deleteListController;
