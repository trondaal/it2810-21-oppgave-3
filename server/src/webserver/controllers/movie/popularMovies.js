const configuration = require('src/config/configuration').getInstance();
const Cache = require('src/tmdb/cache');
const TMDB = require('src/tmdb/tmdb');
const cache = new Cache();
const tmdb = new TMDB(cache, configuration.get('tmdb', 'apiKey'));

/**
 * Controller: Retrieve the most popular movies from TMDB
 * @param {Request} req http request variable
 * @param {Response} res
 * @returns {Callback}
 */
function popularMoviesController(req, res) {
  tmdb.popular()
  .then((movies) => {
    res.send(movies);
  }).catch((error) => {
    res.send(error);
  });
}

module.exports = popularMoviesController;
