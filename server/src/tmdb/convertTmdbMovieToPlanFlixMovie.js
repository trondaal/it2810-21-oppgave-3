const Movie = require('src/movie/movie');

/**
* Returns a Movie from a TMDB object.
* @param {Object} tmdbMovie from TMDB
* @returns {Movie}
*/
function convertTmdbMovieToPlanFlixMovie(tmdbMovie) {
  const movie = new Movie();
  movie.title = tmdbMovie.title;
  movie.id = tmdbMovie.id;
  movie.rank = tmdbMovie.vote_average;
  movie.overview = tmdbMovie.overview;

  if (tmdbMovie.release_date !== undefined) {
    movie.release_date = new Date(tmdbMovie.release_date);
    movie.year = movie.release_date.getFullYear();
  }

  if (tmdbMovie.poster_path !== undefined) {
    if (tmdbMovie.poster_path !== null) {
      movie.poster = `https://image.tmdb.org/t/p/w600${tmdbMovie.poster_path}`;
    } else {
      movie.poster = '/app/img/image_nf.svg';
    }
  }

  if (tmdbMovie.backdrop_path !== undefined) {
    movie.backdrop = `https://image.tmdb.org/t/p/w1920${tmdbMovie.backdrop_path}`;
  }

  return movie;
}

module.exports = convertTmdbMovieToPlanFlixMovie;
