/* eslint-disable consistent-return */
const moviedb = require('moviedb');
const convertTmdbMovieToPlanFlixMovie = require('src/tmdb/convertTmdbMovieToPlanFlixMovie');

class TMDB {

  constructor(cache, apiKey, tmdbLibrary) {
    this.cache = cache;
    this.tmdbLibrary = tmdbLibrary || moviedb(apiKey);
    this.cacheTags = {
      'search': 's:',
      'lookup': 'l:',
      'popular': 'p:',
    };
  }

  /**
   * Retrieve search results from TMDB.
   * @param {String} text query you want to search for
   * @param {Number} page representing pagination of results
   * @param {Boolean} adult include adult movies in the result
   * @returns {Promise} succeeds if results were found
   */
  search(text, page = 1, adult = true) {
    const query = { query: text, page, include_adult: adult };
    const cacheKey = `${this.cacheTags.search}${page}:${adult}:${text}`;
    return Promise.resolve()
    .then(() => this.cache.get(cacheKey))
    .catch(() => this.tmdb('searchMovie', query))
    .catch(() => { throw new Error('Could not search for movies.'); })
    .then(response => this.cache.set(cacheKey, response))
    .then((response) => {
      try {
        return response.results.map(convertTmdbMovieToPlanFlixMovie);
      } catch (parseError) {
        throw new Error('Could not parse results.');
      }
    });
  }

  /**
   * Retrieve popular movies from TMDB.
   * @returns {Promise} succeeds if popular movies were found
   */
  popular() {
    const cacheKey = `${this.cacheTags.popular}`;
    return Promise.resolve()
    .then(() => this.cache.get(this.cacheTags.popular))
    .catch(() => this.tmdb('miscPopularMovies'))
    .catch(() => { throw new Error('Could not find popular movies.'); })
    .then(response => this.cache.set(cacheKey, response))
    .then((response) => {
      try {
        return response.results.map(convertTmdbMovieToPlanFlixMovie);
      } catch (parseError) {
        throw new Error('Could not parse results.');
      }
    });
  }

  /**
   * Retrieve a specific movie by id from TMDB.
   * @param {Number} identifier of the movie you want to retrieve
   * @returns {Promise} succeeds if movie was found
   */
  lookup(identifier) {
    const query = { id: identifier };
    const cacheKey = `${this.cacheTags.lookup}${identifier}`;
    return Promise.resolve()
    .then(() => this.cache.get(cacheKey))
    .catch(() => this.tmdb('movieInfo', query))
    .catch(() => { throw new Error('Could not find a movie with that id.'); })
    .then(response => this.cache.set(cacheKey, response))
    .then((response) => {
      try {
        return convertTmdbMovieToPlanFlixMovie(response);
      } catch (parseError) {
        throw new Error('Could not parse movie.');
      }
    });
  }

  /**
   * Wraps moviedb library to support Promises.
   * @param {String} method function name in the library
   * @param {Object} argument argument to function being called
   * @returns {Promise} succeeds if callback succeeds
   */
  tmdb(method, argument) {
    return new Promise((resolve, reject) => {
      const callback = (error, response) => {
        if (error) {
          return reject(error);
        }
        resolve(response);
      };

      if (!argument) {
        this.tmdbLibrary[method](callback);
      } else {
        this.tmdbLibrary[method](argument, callback);
      }
    });
  }
}

module.exports = TMDB;
