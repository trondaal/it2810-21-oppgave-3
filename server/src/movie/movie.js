class Movie {
  constructor(title) {
    this.id = null;
    this.title = title;
    this.year = null;
    this.overview = null;
    this.poster = null;
    this.rank = null;
    this.release_date = null;
    this.backdrop = null;
  }
}

module.exports = Movie;
