# Planflix server

### Dependencies
To get started the only requirement is that you have ````node (v7.0.0)```` and ````npm```` in your path. 

### Install
To install the libraries the project requires, type this command in the root directory. 

### Configuration
All configurations reside in the ````conf```` directory.

````
npm install
````

### Test
If you would like to confirm that the system works correctly, type this command in the root directory.

````
npm test
````

### Start
To start the webserver on localhost port ````8000````, type this command in the root directory.

````
npm start
````

### Structure

````tree
.
├── docs: documentation on api and classes
├── lib: all the libraries server depends on
├── script: useful scripts
├── src: source code for the project
└── test:
    ├── fixtures: tmdb http responses for testing
    ├── system: high-level use case tests
    └── unit: isolated unit tests
````
