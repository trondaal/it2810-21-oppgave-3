# PlanFlix server API

This document describes layout of the database.

**Databse flie:** server/planflix.db

**Tables:**


User									|
----------------------------------------| -------------
user_token,	varchar(127) PRIMARY KEY	| A unique identifier for the user.
user_name,	varchar(127)				| The users username.
password,	varchar(127)				| The users password (will be tokenized).



Search_history							|
----------------------------------------| -------------
search,		varchar(127) PRIMARY KEY	| A statement the user has searched.
user_token,	varchar(127) PRIMARY KEY	| A unique identifier for the user.



List									|
----------------------------------------| -------------
list_id,	int 		 PRIMARY KEY	| A unique identifier for the list.
name,		varchar(127)				| The lists name.
user_token,	varchar(127)				| A unique identifier for the user.



List_content							|
----------------------------------------| -------------
list_id,	int 		 PRIMARY KEY	| A unique identifier for the list.
movie_id,	int			 PRIMARY KEY	| A unique identifier for the movie equal to TheMovieDatabase's movie id.

