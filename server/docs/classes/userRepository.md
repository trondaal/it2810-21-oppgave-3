
# create

Create a user in a database.

**Parameters**

-   `user` **User** the user you want to create

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 

# retrieveHash

Retrieve a password from a database.

**Parameters**

-   `user` **User** the user you want to retrieve the password

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 

# changePassword

Change a user's password in a database.

**Parameters**

-   `user` **User** the user you want to create
-   `password` **[String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** the new password you want to change

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 
