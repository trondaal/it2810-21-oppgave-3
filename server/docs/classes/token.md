
# toString

Generate a new token.

**Parameters**

-   `secret` **[String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** a cipher of the token

Returns **[String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

# fromString

Decode a token.

**Parameters**

-   `jwtToken` **Token** an encrypted token
-   `secret` **[String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** a cipher of the token

Returns **Token** 
