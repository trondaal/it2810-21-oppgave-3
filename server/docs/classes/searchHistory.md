
# read

Retrive a search queries for a user from the database.

**Parameters**

-   `user` **User** existing user

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 

# create

Creates a new search entry in the database.

**Parameters**

-   `user` **User** a new user
-   `searchQuery` **[String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** the query the user searched for

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 
