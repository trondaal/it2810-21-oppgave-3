
# allByUser

Retrieves all the lists for a user.

**Parameters**

-   `user` **User** existing user

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if lists were found

# create

Create a new list in the database.

**Parameters**

-   `user` **User** existing user
-   `list` **List** that will be created

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was created

# read

Retrieve a list that belongs to a user.

**Parameters**

-   `user` **User** user that owns the list
-   `listId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the list you want retrieved

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was found

# remove

Remove a list that belongs to a user.

**Parameters**

-   `user` **User** that owns the list
-   `listId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the list you want removed

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was removed

# readMoviesInList

Retrieve the movies in a list.

**Parameters**

-   `user` **User** of the user that owns the list
-   `listId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the list you want to get movies out of

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was removed

# addMovie

Add a movie to a list belonging to a user.

**Parameters**

-   `user` **User** of the user that owns the list
-   `listId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the list you want to add a movie to
-   `movieId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the movie you want to add

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was removed

# removeMovie

Remove a movie to a list belonging to a user.

**Parameters**

-   `user` **User** of the user that owns the list
-   `listId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the list you want to remove a movie from
-   `movieId` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** for the movie you want to remove

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** succeeds if list was removed
