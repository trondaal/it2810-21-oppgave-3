
# post

User

# get

Movie

# get

List

# registerController

Controller: Register a new user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# loginController

Controller: Log in a user provided correct credentials.

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# historyController

Controller: Retrieves search history of a logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# popularMoviesController

Controller: Retrieve the most popular movies from TMDB

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# searchMoviesController

Controller: Search for movies by query, page and optionally adult

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# readMovieController

Controller: Retrieve information for a movie

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# readListsController

Controller: Retireve all lists of the logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# createListController

Controller: Create a list for the logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# moviesInListController

Controller: Retireve all the movies in a list belonging to the logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# addMovieToListController

Controller: Add a movie to a list.

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# deleteListController

Controller: Delete a list that belongs to the logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 

# deleteMovieController

Controller: Remove a movie from a list belonging to the logged in user

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 
