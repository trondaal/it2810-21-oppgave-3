
# add

Add a movie to the list.

**Parameters**

-   `movie` **Movie** the movie you want to add

Returns **[undefined](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined)** 

# get

Get a movie from the list.

**Parameters**

-   `index` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** of the movie you want to get

Returns **Movie** 

# length

Get a length of the list.

Returns **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** 
