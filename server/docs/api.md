# PlanFlix server API

This document describes all the available HTTP endpoints the server exposes.

## POST /api/v1/list

### Should return 401
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
name  |  Space movies



**Headers:**

Key           | Value
------------- | -------------
authorization  |  maLfOrMed TOKEN ...
content-type  |  application/json

### Response (HTTP status 401)
````javascript
{ success: false, error: 'You must be logged in.' }
````

### Should return 200 when list gets created
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
name  |  Space movies



**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...
content-type  |  application/json

### Response (HTTP status 200)
````javascript
{ success: true }
````





## POST /api/v1/list/1

### Should return 200 when list gets created
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
movieId  |  1



**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...
content-type  |  application/json

### Response (HTTP status 200)
````javascript
{ success: true }
````





## DELETE /api/v1/list/1

### Should return 200 when list gets deleted
**Method:** `DELETE`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
{ success: true }
````





## GET /api/v1/movie/1

### Should return 200 with the information
**Method:** `GET`


### Response (HTTP status 200)
````javascript
{ id: 157336,
  title: 'Interstellar',
  year: 2014,
  overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
  poster: 'https://image.tmdb.org/t/p/w600/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
  rank: 8.1,
  release_date: '2014-11-05T00:00:00.000Z',
  backdrop: 'https://image.tmdb.org/t/p/w1920/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg' }
````





## GET /api/v1/movie/popular

### Should return 200 with the information
**Method:** `GET`


### Response (HTTP status 200)
````javascript
[ { id: 284052,
    title: 'Doctor Strange',
    year: 2016,
    overview: 'After his career is destroyed, a brilliant but arrogant surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.',
    poster: 'https://image.tmdb.org/t/p/w600/cPaQPtbJkJzuDQ6BxyH9Dg4Fw8r.jpg',
    rank: 7.36,
    release_date: '2016-10-25T00:00:00.000Z',
    backdrop: 'https://image.tmdb.org/t/p/w1920/hETu6AxKsWAS42tw8eXgLUgn4Lo.jpg' } ]
````





## POST /api/v1/user/login

### Should return 200 with a token if correct credentials are given
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
username  |  Ping
password  |  password



**Headers:**

Key           | Value
------------- | -------------
content-type  |  application/json

### Response (HTTP status 200)
````javascript
{ success: true,
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBpbmciLCJpYXQiOjE0NzkwMjkzMzF9.OSRZSpoR-JN28-epGqww32g8mhahzOFjw0lQ8AUQIio' }
````

### Should return 401 if incorrect credentials are given
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
username  |  Ping
password  |  anti-password



**Headers:**

Key           | Value
------------- | -------------
content-type  |  application/json

### Response (HTTP status 401)
````javascript
{ success: false, error: 'Wrong username or password.' }
````





## POST /api/v1/user

### Should return 200 and a message indicating success
**Method:** `POST`

**Parameters:**

Key           | Value
------------- | -------------
username  |  Ping
email  |  ping@gmail.com
password  |  password



**Headers:**

Key           | Value
------------- | -------------
content-type  |  application/json

### Response (HTTP status 200)
````javascript
{ success: true, message: 'Welcome to PlanFlix' }
````





## DELETE /api/v1/list/1/2

### Should return 200 when list gets created
**Method:** `DELETE`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
{ success: true }
````





## GET /api/v1/movie/search?query=interstellar&page=1&adult=false

### Should return 200 with the search results
**Method:** `GET`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
[ { id: 157336,
    title: 'Interstellar',
    year: 2014,
    overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
    poster: 'https://image.tmdb.org/t/p/w600/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
    rank: 8.1,
    release_date: '2014-11-05T00:00:00.000Z',
    backdrop: 'https://image.tmdb.org/t/p/w1920/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg' } ]
````





## GET /api/v1/list

### Should return 200 with an array of the lists
**Method:** `GET`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
[ { name: 'Favorites', id: 1 } ]
````





## GET /api/v1/list/1

### Should return 200 with an array of the movies
**Method:** `GET`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
[ { id: 157336,
    title: 'Interstellar',
    year: 2014,
    overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
    poster: 'https://image.tmdb.org/t/p/w600/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
    rank: 8.1,
    release_date: '2014-11-05T00:00:00.000Z',
    backdrop: 'https://image.tmdb.org/t/p/w1920/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg' } ]
````





## GET /api/v1/user/history

### Should return 200 with the search results
**Method:** `GET`

**Headers:**

Key           | Value
------------- | -------------
authorization  |  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ ...

### Response (HTTP status 200)
````javascript
{ success: true, searchQueries: [ 'interstellar' ] }
````





## GET /api/v1/movie/search?query=interstellar&page=1

### Should return 200 with the search results even if user is not logged in
**Method:** `GET`


### Response (HTTP status 200)
````javascript
[ { id: 157336,
    title: 'Interstellar',
    year: 2014,
    overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
    poster: 'https://image.tmdb.org/t/p/w600/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
    rank: 8.1,
    release_date: '2014-11-05T00:00:00.000Z',
    backdrop: 'https://image.tmdb.org/t/p/w1920/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg' } ]
````





