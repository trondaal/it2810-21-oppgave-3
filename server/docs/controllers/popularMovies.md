
# popularMoviesController

Controller: Retrieve the most popular movies from TMDB

**Parameters**

-   `req` **[Request](https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/request)** http request variable
-   `res` **[Response](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)** 

Returns **Callback** 
