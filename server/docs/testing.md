# Testing

This document describes how the group approached testing of the project. 

## Previous experience

The group members had a varying degree of experience with testing. Some had no experience while others had  experience in practicing disciplines like TDD. 

None the less, it was unanimously agreed that testing was something that would provide confidence and regression to the system. This would in turn speed up the development.

To ensure that everyone had been introduced to the concepts, a couple of hours were allocated to teaching TDD . Teaching consisted of pair programming use cases related to persistence (ie. `listRepository` & `movieRepository`).

The goal of this phase was not to make every group member an expert within the domain of testing, but rather provide some common ground of understanding to use as a foundation.

## Automated vs. manual tests

Usually tests fall within one of two categories: Automated or manual. A test in the former category can be, and usually are, orders of magnitude more expensive than an automated one.

The reason for this difference in cost is almost self-explanatory: Manual tests require manual work. Automated tests, on the other hand, can be executed by a computer.

Manual tests often entail conducting an experiment with a human subject and observing how the subject completes a set of steps. An automated test does not involve any subject, or a human for that matter. It is entirely automated and can be ran without any physical effort.

## Strategy

The Javascript community is known for its extremly high churn rate regarding new frontend frameworks and libraries. On these grounds the group chose to let the server leverage as much of the business logic as possible, so that the frontend could easily be replaced whenever the next shiny new framework came around. This meant that the frontend contained very few tests, while the backend contained many.

## Continous integration

When a group member pushed code to BitBucket the automated tests were automatically ran. This made sure that no code could be added, removed or changed without confirming that it did not break our build.

This gave the group members confidence in their changes. Relieving the fear of refactoring, improving the design and making it safer to upgrade new dependencies.

## In practice

### Running tests

To run the tests a group member simply had to type `npm test` in the command line inside the `server/` directory. If all the tests passed the following output was produced.

````bash
$ npm test

․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․

85 passing (151ms)
````

If the code was altered in a way that failed the tests, the output would look something like the following.

````bash
$ npm test

․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․!․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․

85 passing (162ms)
1 failing

1) List should have length of 0 if no movies have been added:
     
AssertionError: 1 == 0
+ expected - actual
````
 
### Writing tests

All the tests resided in the `server/test/unit` directory. This directory mirrored the directory structure of the `server/src` directory so that one could easily navigate tests by knowing the component.

The following test is taken from `server/test/unit/list/testListRepository`. It asserts that a row representing the list is stored in the database when a list (called *Space movies*) is given to an instance of the `ListRepository` class.


````javascript
it('should create a list', () => {
  const listRepository = new ListRepository(this.database);
  const spaceMovies = new List('Space movies');
  return Promise.resolve()
  .then(() => listRepository.create(spaceMovies))
  .then(() => this.database.get('select count(*) as count from list'))
  .then(row => assert.equal(row.count, 1));
});
````

### Linting

Linting was used to ensure that the style of the code was consistent throughout the entire code base. To run the linter a group member only had to type `npm run lint` in the command line. The linter would exit with a non-zero signal if the code did not fulfill best practices and consistency. For instance, if a semi colon were missing the linter would produce the following output.

````bash
$ npm run lint

~/it2810-21-oppgave-3/server/src/list/list.js
  17:30  error  Missing semicolon  semi

✖ 1 problem (1 error, 0 warnings)
````

### Coverage

To ensure that as many lines of source code was governed by tests, Istanbul was used for collecting code coverage. Istanbul would run the tests and monitor which lines of the source code were being executed. The higher the percentage, the better.

The following image shows the coverage for `server/src/webserver/controllers/moviesInList.js`. The red line on line 12 means that the tests do not cover that particular line. In other words, there are no tests testing the "sad path" in the `moviesInListController`.

![coverage result](img/coverage.png)
