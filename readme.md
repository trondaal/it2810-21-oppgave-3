# [PlanFlix - Your personal movie diary](http://it2810-21.idi.ntnu.no/)

> A web application that enables users to keep track of which movies they have seen and which movies
they plan to see.

## Build information 

Property | Status
---------|--------
 Build status  | [![](https://img.shields.io/codeship/c691d010-4ea2-0134-7d49-468259afc5dd/master.svg)](https://app.codeship.com/projects/170771) |
 Test coverage | [![Coverage Status](https://coveralls.io/repos/bitbucket/trondaal/it2810-21-oppgave-3/badge.svg?branch=master)](https://coveralls.io/bitbucket/trondaal/it2810-21-oppgave-3?branch=master)
 Deployment | [![](https://img.shields.io/badge/it2810%E1%A0%8621.idi.ntnu.no-running-green.svg)](http://it2810-21.idi.ntnu.no)


## Screenshots

List of movies           | A single movie           | Popular movies
:-----------------------:|:------------------------:|:----------------------------------------------------------------:
![](client/docs/img/list-of-movies-min.png) | ![](client/docs/img/single-movie-min.png) | ![](client/docs/img/popular-min.png)

## Testing

The project is heavily tested with unit, integration and system tests. When code is addded, changed or deleted, the tests are automatically executed through a continous integration (CI) server and reported as *Build status* in the table above. Please read our short [document](server/docs/testing.md) on how the tests are written.

There are 151 tests covering 96% of the system. It takes 5 seconds to execute all of them. This means you can refactor and add new functionality without fearing that the system will break. You can look at the tests [here](server/test).

````
$ npm test
․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․,
․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․․
․․․․․․․․․․․․․․․․․․․,․․․․․․․․․․․․․․․

  151 passing (5s)
  2 pending
```` 

### Test plan

The test plan relates the requirements, expressed as user stories, with the automated tests. ST is an abbrevation for "system test". Also known as high level use case test.

| Test ID | User story | Precondition | Test | Fulfilled | 
|:-------:|------------|:------------:|------|:---------:|
| ST1 | As a user I want to register. | | [`asAUserIWantToRegister.js`](server/test/system/asAUserIWantToRegister.js)| ✓ |
| ST2 | As a user I want to log in. | ST1 | [`asAUserIWantToLogIn.js`](server/test/system/asAUserIWantToLogIn.js)| ✓ |
| ST3 | As a user I want to search for a movie. | | [`asAUserIWantToSearchForAMovie.js`](server/test/system/asAUserIWantToSearchForAMovie.js) | ✓ |
| ST4 | As a user I want to create a list. | ST2 | [`asAUserIWantToCreateAList.js`](server/test/system/asAUserIWantToCreateAList.js) | ✓ |
| ST5 | As a user I want to add a movie to a list. | ST4 | [`asAUserIWantToAddAMovieToAList.js`](server/test/system/asAUserIWantToAddAMovieToAList.js) | ✓ |
| ST6 | As a user I want to remove a movie from a list. | ST5 | [`asAUserIWantToRemoveAMovieFromAList.js`](server/test/system/asAUserIWantToRemoveAMovieFromAList.js) | ✓ |
| ST7 | As a user I want to see the movies in a list. | ST2 | [`asAUserIWantToSeeTheMoviesInAList.js `](server/test/system/asAUserIWantToSeeTheMoviesInAList.js) | ✓ |
| ST8 | As a user I want to see what I have searched for. | ST2, ST3 | [`asAUserIWantToSeeWhatIHaveSearchedFor.js `](server/test/system/asAUserIWantToSeeWhatIHaveSearchedFor.js) | ✓ |


## Documentation

The application programming interface (API) the server exposes is thourughly documented with detailed examples of requests and responses [here](server/docs/api.md). Furthermore, all methods and classes are documented according to the [JSDoc 3](http://usejsdoc.org) standard. These comments are available as Markdown documents in [this](server/docs/classes) directory as well. 

## Structure

The application is divided into two parts: Client and server. The client communicates with the server and the serves
responds accordingly. They are two separate projects with each own `package.json` file.


### Architechture

The system is designed such that all the business logic of PlanFlix is located on the server. The client is almost completely stateless, while the server handles all authentication, caching, searching and so on.

The rationale behind this design decision was to avoid duplicating input validation, isolate the core business logic from third party frameworks (like Angular) and to make it easier to cache data.

We used test-driven development (TDD) to drive the architecture of the server. Everything was built piece by piece and later assembled together. It is trivially easy to replace any of the layers in the application due to their isolation.

### Client
The client directory contains an Angular2 application and a directory of static HTML and CSS files. The client communicates with the server over Hypertext Transfer Protocol (HTTP). The data format that is used
is Javascript objects (also known as JSON).

Please see the client [readme](client/) for further information.

### Server
The server directory contains a Node project that exposes HTTP endpoints to the client. The controller delegates the request to service classes which in turn communicates with different repositories.

Please see the server [readme](server/) for further information.


Communication          | Architecture
-----------------------|-------------------------
This figure describes how the client and server communicates and how the server requests metadata for movies from TheMovieDB. | This is an accurate description of the architecture on the server. Note that the cache greatly reduces the amounts of requests to TheMovieDB. 
![](client/docs/img/communication.png) | ![](server/docs/img/architecture.png)